package main.java.com.getjavajob.training.algo07.velikohatkon.lesson10;

/**
 * Created by Николай on 29.02.2016
 * at 16:13
 */
public class BubbleSort<E> {

    public void sorting(E[] arr) {
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if(arr[j].hashCode() > arr[j + 1].hashCode()){
                    E buff = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = buff;
                }
            }
        }
    }

}
