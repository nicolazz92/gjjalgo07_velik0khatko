package main.java.com.getjavajob.training.algo07.velikohatkon.lesson09;

import main.java.com.getjavajob.training.algo07.util.Assert;

import java.util.*;

/**
 * Created by ??????? on 30.01.2016.
 * on 19:40
 */
public class SortedSetTest {

    private static SortedSet<Integer> set = new TreeSet<>();

    private static void setInit() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        set.add(7);
        set.add(8);
        set.add(9);
        set.add(10);
    }

    public static void subSetTest() {

        //general test
        Set<Integer> sub = set.subSet(3, 9);
        List<Integer> list = new LinkedList<>(sub);
        Assert.assertEquals(list.toString(), "[3, 4, 5, 6, 7, 8]");


        //ClassCastException test
        TreeSet<Integer> noComp = new TreeSet<>();
        noComp.add(1);
        noComp.add(2);
        noComp.add(3);
        noComp.add(4);
        noComp.add(5);
        try {
            noComp.subSet(1, 4);
        } catch (ClassCastException e) {
            System.out.println(e.getMessage());
        }

        //NullPointerException test
        try {
            sub = set.subSet(2, 12);
            new LinkedList<>(sub);
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }

        //NullPointerException test
        try {
            sub = set.subSet(12, 2);
            new LinkedList<>(sub);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "fromKey > toKey");
        }

    }

    public static void headSetTest() {
        //general test
        SortedSet<Integer> head = set.headSet(5);
        List<Integer> list = new LinkedList<>(head);
        Assert.assertEquals("[1, 2, 3, 4]", list.toString());
    }

    public static void tailSetTest() {
        //general test
        SortedSet<Integer> tail = set.tailSet(5);
        List<Integer> list = new LinkedList<>(tail);
        Assert.assertEquals(list.toString(), "[5, 6, 7, 8, 9, 10]");
    }

    public static void firstTest() {
        //general test
        Object tail = set.first();
        Assert.assertEquals(tail, 1);
    }

    public static void lastTest() {
        //general test
        Object tail = set.last();
        Assert.assertEquals(tail, 10);
    }

    public static void main(String[] args) {
        setInit();
        subSetTest();
        headSetTest();
        tailSetTest();
        firstTest();
        lastTest();
    }
}
