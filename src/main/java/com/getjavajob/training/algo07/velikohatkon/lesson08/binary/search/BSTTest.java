package main.java.com.getjavajob.training.algo07.velikohatkon.lesson08.binary.search;

import main.java.com.getjavajob.training.algo07.util.Assert;

/**
 * Created by ??????? on 25.01.2016.
 * on 10:28
 */
public class BSTTest {
    public static void treeSearchTest() {
        BinarySearchTree<Integer> d = new BinarySearchTree<>();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);
        d.add(75);
        d.add(125);
        d.add(175);

        Assert.assertEquals(d.treeSearch(d.getRoot(), 125).getElement(), new Integer(125));
        Assert.assertEquals(d.treeSearch(d.getRoot(), 175).getElement(), new Integer(175));
        Assert.assertEquals(d.treeSearch(d.getRoot(), 50).getElement(), new Integer(50));
        Assert.assertEquals(d.treeSearch(d.getRoot(), 100).getElement(), new Integer(100));
        Assert.assertEquals(d.treeSearch(d.getRoot(), 15), null);
    }

    public static void main(String[] args) {
        treeSearchTest();
    }
}
