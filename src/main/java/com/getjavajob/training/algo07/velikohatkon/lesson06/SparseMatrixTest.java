package main.java.com.getjavajob.training.algo07.velikohatkon.lesson06;

import main.java.com.getjavajob.training.algo07.util.Assert;

/**
 * Created by ������� on 28.11.2015.
 * on 14:33
 */
public class SparseMatrixTest {
    public static SparseMatrix d = new SparseMatrix();

    public static void addTest() {
        Assert.assertEquals(d.add(2, 3, 12), null);
        Assert.assertEquals(d.add(2, 3, 13), new Integer(12));
        Assert.assertEquals(d.add(1_000_123, 423, 7432), null);
        Assert.assertEquals(d.add(1_000_123, 423, 123), new Integer(7432));
        for (int i = 0; i < 10_000; i++) {
            if (i % 1_000 == 0.0) {
                Assert.assertEquals(d.add(1_999_999, i, i), null);
            } else {
                d.add(1_999_999, i, i);
            }
        }
    }

    public static void getTest() {
        for (int i = 0; i < 10_000; i += 1_000) {
            Assert.assertEquals(d.get(1_999_999, i), new Integer(i));
        }
    }

    public static void removeTest(){
        for(int i = 0; i < 10_000; i++){
            if(i % 1_000 == 0.0){
                Assert.assertEquals(d.remove(1_999_999, i), new Integer(i));
            } else {
                d.remove(1_999_999, i);
            }
        }
    }

    public static void getMatrixTest(){
        System.out.println(d.getMatrix().hashCode());
    }

    public static void main(String[] args) {
        addTest();
        getTest();
        removeTest();
        getMatrixTest();
    }
}
