package main.java.com.getjavajob.training.algo07.velikohatkon.lesson01;

/**
 * Created by ������� on 11.08.2015.
 * on 10:46
 */
public class Task07 {
    int a;
    int b;

    Task07(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public void flipAr1() {
        a += b;
        b = a - b;
        a -= b;
        System.out.println(Integer.toString(a, 2) + " " + Integer.toString(b, 2));
    }

    public void flipAr2() {
        a *= b;
        b = a / b;
        a /= b;
        System.out.println(Integer.toString(a, 2) + " " + Integer.toString(b, 2));
    }

    public void flipBit1() {
        a = a ^ b;
        b = a ^ b;
        a = a ^ b;
        System.out.println(Integer.toString(a, 2) + " " + Integer.toString(b, 2));
    }

    public void flipBit2() {
        int mask1 = 0b01111111111111111111111111111111;
        int mask2 = 0b00000000000000000000000000000001;
        for (int i = 0; i < 32; i++) {
            a = (a & mask1) | ((b & mask2) << 31);
            if (i != 31) {
                b = (b & mask1) | ((a & mask2) << 31);
                a >>>= 1;
                b >>>= 1;
            }
        }
    }

    public void test(int a, int b) {
        System.out.println(this.a == a && this.b == b);
    }
}
