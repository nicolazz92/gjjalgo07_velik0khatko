package main.java.com.getjavajob.training.algo07.velikohatkon.lesson08.binary.search.balanced;

import main.java.com.getjavajob.training.algo07.util.Assert;
import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;

import java.util.Iterator;

/**
 * Created by ??????? on 26.01.2016.
 * on 11:15
 */
public class BalanceableTreeTest {

    public static void rightRotateNoRootTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(100);
        d.add(150);
        d.add(50);
        d.add(25);
        d.add(75);
        d.add(15);
        d.add(35);

        System.out.println("tree created");

        d.rotate(d.treeSearch(d.root(), 25));

        Iterator<Node<Integer>> iter = d.iterator("width");
        String a = "";
        while (iter.hasNext()) {
            a += iter.next().getElement() + " ";
        }
        Assert.assertEquals(a, "100 25 150 15 50 35 75 ");

        System.out.println("tree rotated");
    }

    public static void leftRotateNoRootTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(100);
        d.add(150);
        d.add(50);
        d.add(125);
        d.add(175);
        d.add(160);
        d.add(195);

        System.out.println("tree created");

        d.rotate(d.treeSearch(d.root(), 175));

        Iterator<Node<Integer>> iter = d.iterator("width");
        String a = "";
        while (iter.hasNext()) {
            a += iter.next().getElement() + " ";
        }
        Assert.assertEquals(a, "100 50 175 150 195 125 160 ");

        System.out.println("tree rotated");
    }

    public static void rightRotateRootTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(100);
        d.add(150);
        d.add(50);
        d.add(25);
        d.add(75);
        d.add(15);
        d.add(35);

        System.out.println("tree created");

        d.rotate(d.treeSearch(d.root(), 50));

        Iterator<Node<Integer>> iter = d.iterator("width");
        String a = "";
        while (iter.hasNext()) {
            a += iter.next().getElement() + " ";
        }
        Assert.assertEquals(a, "50 25 100 15 35 75 150 ");

        System.out.println("tree rotated");
    }

    public static void leftRotateRootTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(100);
        d.add(150);
        d.add(50);
        d.add(125);
        d.add(175);
        d.add(160);
        d.add(195);

        System.out.println("tree created");

        d.rotate(d.treeSearch(d.root(), 150));

        Iterator<Node<Integer>> iter = d.iterator("width");
        String a = "";
        while (iter.hasNext()) {
            a += iter.next().getElement() + " ";
        }
        Assert.assertEquals(a, "150 100 175 50 125 160 195 ");

        System.out.println("tree rotated");
    }

    public static void leftRotate2elemTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(100);
        d.add(150);

        System.out.println("tree created");

        d.rotate(d.treeSearch(d.root(), 150));

        Iterator<Node<Integer>> iter = d.iterator("width");
        String a = "";
        while (iter.hasNext()) {
            a += iter.next().getElement() + " ";
        }
        Assert.assertEquals(a, "150 100 ");

        System.out.println("tree rotated");
    }

    public static void rightRotate2elemTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(100);
        d.add(50);

        System.out.println("tree created");

        d.rotate(d.treeSearch(d.root(), 50));

        Iterator<Node<Integer>> iter = d.iterator("width");
        String a = "";
        while (iter.hasNext()) {
            a += iter.next().getElement() + " ";
        }
        Assert.assertEquals(a, "50 100 ");

        System.out.println("tree rotated");
    }

    public static void rotateBagTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(100);

        System.out.println("tree created");

        try {
            d.rotate(d.treeSearch(d.root(), 100));
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Argument node has not parent");
        }

        System.out.println("tree rotated");
    }

    public static void rotateTwiceLeftRightTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(1000);
        d.add(1500);
        d.add(500);
        d.add(250);
        d.add(750);
        d.add(600);
        d.add(900);

        System.out.println("tree created");

        d.rotateTwice(d.treeSearch(d.root(), 750));

        Iterator<Node<Integer>> iter = d.iterator("width");
        String a = "";
        while (iter.hasNext()) {
            a += iter.next().getElement() + " ";
        }
        Assert.assertEquals(a, "750 500 1000 250 600 900 1500 ");

        System.out.println("tree rotated");
    }

    public static void rotateTwiceRightLeftTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(1000);
        d.add(500);
        d.add(1500);
        d.add(3000);
        d.add(1250);
        d.add(1300);
        d.add(1100);

        System.out.println("tree created");

        d.rotateTwice(d.treeSearch(d.root(), 1250));

        Iterator<Node<Integer>> iter = d.iterator("width");
        String a = "";
        while (iter.hasNext()) {
            a += iter.next().getElement() + " ";
        }
        Assert.assertEquals(a, "1250 1000 1500 500 1100 1300 3000 ");

        System.out.println("tree rotated");
    }

    public static void rotateTwiceNoGrandBagTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(1000);
        d.add(500);

        System.out.println("tree created");

        try {
            d.rotateTwice(d.treeSearch(d.root(), 500));
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Node has not grandfather");
        }

        System.out.println("tree rotated");
    }

    public static void rotateTwiceOneSideBagTest() {
        BalanceableTree<Integer> d = new BalanceableTree<>();

        d.add(1000);
        d.add(500);
        d.add(200);

        System.out.println("tree created");

        try {
            d.rotateTwice(d.treeSearch(d.root(), 200));
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "Opposite rotations doesn't possible");
        }

        System.out.println("tree rotated");
    }

    public static void main(String[] args) {
        rightRotateNoRootTest();
        leftRotateNoRootTest();
        rightRotateRootTest();
        leftRotateRootTest();
        leftRotate2elemTest();
        rightRotate2elemTest();
        rotateBagTest();
        rotateTwiceLeftRightTest();
        rotateTwiceRightLeftTest();
        rotateTwiceNoGrandBagTest();
        rotateTwiceOneSideBagTest();
    }
}