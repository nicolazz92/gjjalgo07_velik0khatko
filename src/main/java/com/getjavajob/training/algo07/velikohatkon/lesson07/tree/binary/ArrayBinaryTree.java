package main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.binary;

import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class ArrayBinaryTree<E> extends AbstractBinaryTree<E> {

    private ArrayList<Node<E>> data = new ArrayList<>();
    private int size = 0;
    private int index = 0;

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean add(E d) {
        growIfLittle(2);
        NodeImpl<E> elem = validate(data.get(0));
        if (isEmpty()) {
            addRoot(d);
            size++;
            return true;
        }
        while (elem.getElement() != null) {
            if (d.hashCode() < elem.getElement().hashCode()) {
                if (getLeft(elem) != null) {
                    elem = validate(left(elem));
                    continue;
                }
                addLeft(elem, d);
            } else if (d.hashCode() > elem.getElement().hashCode()) {
                if (getRight(elem) != null) {
                    elem = validate(right(elem));
                    continue;
                }
                addRight(elem, d);
            } else if (d.hashCode() == elem.getElement().hashCode()) {
                return false;
            }
        }
        return true;
    }

    private void growIfLittle(int n){
        if(data.size() < n){
            int count = n - data.size();
            for(int i = 0; i <= count + 1; i++){
                data.add(null);
            }
        }
    }

    @Override
    public boolean isEmpty() {
        growIfLittle(1);
        if (data.get(0) == null) {
            data.clear();
            return true;
        } else {
            return false;
        }
    }

    public Node<E> getLeft(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> d = validate(p);
        growIfLittle(2 * d.getIndex() + 1);
        return data.get(2 * d.getIndex() + 1);
    }

    public Node<E> getRight(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> d = validate(p);
        growIfLittle(2 * d.getIndex() + 2);
        return data.get(2 * d.getIndex() + 2);
    }

    protected NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        try {
            return (NodeImpl<E>) n;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> d = validate(p);
        index = 2 * d.getIndex() + 1;
        growIfLittle(index);
        return data.get(index);
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        NodeImpl<E> d = validate(p);
        index = 2 * d.getIndex() + 2;
        growIfLittle(index);
        return data.get(index);
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        try {
            NodeImpl<E> d = validate(n);
            if (getLeft(d) == null) {
                NodeImpl<E> a = new NodeImpl<>(e, 2 * d.getIndex() + 1, d.getIndex());
                growIfLittle(a.getIndex());
                data.set(a.getIndex(), a);
                size++;
                return data.get(a.getIndex());
            }
        } catch (IllegalArgumentException error) {
            System.out.println("wrong arg type");
            return null;
        }
        return null;
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        try {
            NodeImpl<E> d = validate(n);
            if (getRight(d) == null) {
                NodeImpl<E> a = new NodeImpl<>(e, 2 * d.getIndex() + 2, d.getIndex());
                growIfLittle(a.getIndex());
                data.set(a.getIndex(), a);
                size++;
                return data.get(a.getIndex());
            }
        } catch (IllegalArgumentException error) {
            System.out.println("wrong arg type");
            return null;
        }
        return null;
    }

    @Override
    public Node<E> root() {
        growIfLittle(2);
        setIndex(0);
        return data.get(0);
    }

    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        if (validate(n).getParentIndex() >= 0) {
            setIndex(validate(n).getParentIndex());
            return data.get(index);
        } else {
            throw new ArrayIndexOutOfBoundsException("This is root, has no parent");
        }
    }

    public Node<E> getParent(Node<E> n) throws IllegalArgumentException {
        if (validate(n).getParentIndex() >= 0) {
            return data.get(validate(n).getParentIndex());
        } else {
            return null;
        }
    }

    @Override
    public Node<E> addRoot(E e) throws IllegalStateException {
        NodeImpl<E> a = new NodeImpl<>(e, 0, -1);
        growIfLittle(2);
        data.set(0, a);
        return data.get(0);
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        throw new UnsupportedOperationException("I could not implement it");
    }

    @Override
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        validate(n).setData(e);
        return validate(n).getElement();
    }

    @Override
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> d = validate(n);
        if(d.getIndex() == 0){
            E res = d.getElement();
            data.clear();
            return res;
        }
        if (getLeft(d) != null) {
            set(left(n), null);
        }
        if (getRight(d) != null) {
            set(right(n), null);
        }
        E res = d.getElement();
        setIndex(d.getParentIndex());
        data.set(d.getIndex(), null);
        return res;
    }

    @Override
    public int size() {
        return size;
    }

    public NodeImpl<E> get(){
        return validate(data.get(index));
    }

    private ArrBinTreeIterator<E> iterator;

    @Override
    public Iterator<E> iterator() {
        if (iterator == null) {
            iterator = new ArrBinTreeIterator<>();
        }
        return iterator;
    }

    @Override
    public Iterable<Node<E>> nodes() {
        return data;
    }

    protected static class ArrBinTreeIterator<E> implements Iterator{
        private ArrayBinaryTree<E> d = new ArrayBinaryTree<>();

        public boolean add(E n) {
            return d.add(n);
        }

        public boolean hasRoot() {
            return d.root() != null;
        }

        public NodeImpl<E> root() {
            d.root();
            return d.get();
        }

        public boolean hasLeft() {
            return d.getLeft(d.get()) != null;
        }

        public NodeImpl left() {
            return d.validate(d.left(d.get()));
        }

        public boolean hasRight() {
            return d.getRight(d.get()) != null;
        }

        public NodeImpl right() {
            return d.validate(d.right(d.get()));
        }

        public boolean hasParent() {
            return d.getParent(d.get()) != null;
        }

        public NodeImpl parent() {
            return d.validate(d.parent(d.get()));
        }

        @Override
        public boolean hasNext() {
            throw new UnsupportedOperationException();
        }

        @Override
        public Object next() {
            throw new UnsupportedOperationException();
        }

        public void remove() {
            d.remove(d.get());
        }

        public int size() {
            return d.size();
        }

        public E getValue() {
            return d.get().getElement();
        }
    }

    protected static class NodeImpl<E> implements Node<E> {
        private E data;
        private int index;
        private int parentIndex;

        public NodeImpl(E data, int index, int parentIndex) {
            this.data = data;
            this.index = index;
            this.parentIndex = parentIndex;
        }

        public int getIndex() {
            return index;
        }

        public int getParentIndex() {
            return parentIndex;
        }

        public void setData(E data) {
            this.data = data;
        }

        @Override
        public E getElement() {
            return data;
        }
    }
}
