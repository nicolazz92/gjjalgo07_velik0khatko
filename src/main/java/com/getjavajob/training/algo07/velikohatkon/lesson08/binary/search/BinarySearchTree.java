package main.java.com.getjavajob.training.algo07.velikohatkon.lesson08.binary.search;

import com.sun.istack.internal.Nullable;
import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;
import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.binary.LinkedBinaryTree;

/**
 * @author Vital Severyn
 * @since 31.07.15
 */
public class BinarySearchTree<E> extends LinkedBinaryTree<E> {

    public BinarySearchTree() {
    }

    /**
     * Method for comparing two values
     *
     * @param val1 first val
     * @param val2 second val
     * @return 1 if val1 > val2
     * 0 if val1 = val2
     * -1 if val1 < val2
     */
    protected int compare(E val1, E val2) {
        if (val1.equals(val2)) {
            return 0;
        } else {
            return val1.hashCode() > val2.hashCode() ? 1 : -1;
        }
    }

    /**
     * Returns the node in n's subtree by val
     *
     * @param n   - root of search subtree
     * @param val - searching node's value
     * @return node if found, null if not
     */
    @Nullable
    public Node<E> treeSearch(Node<E> n, E val) {
        NodeImpl<E> node = validate(n);
        if (compare(val, node.getElement()) == 0) {
            return n;
        } else if (compare(val, n.getElement()) < 0) {
            if (node.getLeft() != null) {
                Node<E> c = node.getLeft();
                return treeSearch(c, val);
            }
        } else if (compare(val, n.getElement()) > 0) {
            if (node.getRight() != null) {
                Node<E> c = node.getRight();
                return treeSearch(c, val);
            }
        }
        return null;
    }

    @Override
    public boolean add(E d) {
        NodeImpl<E> elem = validate(getRoot());
        if (isEmpty()) {
            addRoot(d);
            sizePlus();
            return true;
        }
        while (elem.getElement() != null && treeSearch(getRoot(), d) == null) {
            if (d.hashCode() < elem.getElement().hashCode()) {
                if (elem.getLeft() != null) {
                    elem = validate(elem.getLeft());
                    continue;
                }
                if (treeSearch(getRoot(), d) == null) {
                    addLeft(elem, d);
                }
            } else if (d.hashCode() > elem.getElement().hashCode()) {
                if (elem.getRight() != null) {
                    elem = validate(elem.getRight());
                    continue;
                }
                if (treeSearch(getRoot(), d) == null) {
                    addRight(elem, d);
                }
            } else if (d.hashCode() == elem.getElement().hashCode()) {
                return false;
            }
        }
        return true;
    }
}
