package main.java.com.getjavajob.training.algo07.velikohatkon.lesson08.binary.search.balanced;

import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;

import java.util.*;

/**
 * Created by Николай on 01.02.2016.
 * on 9:31
 */
public class TreeDrawer<E> extends BalanceableTree<E> {

    public List<String> treeLevelStruct;

    /**
     * @param l - level of this string in array structure of tree.
     *          Function calculate a needs number of spaces BEFORE a value and add it to string
     * @return changed a
     */
    public String addSpaceBefore(int l) {
        StringBuilder s = new StringBuilder("");
        int n = (int) (Math.pow(2, height(root()) - l) - 1);
        for (int i = 0; i < n; i++) {
            s.append(" ");
        }
        return s.toString();
    }

    /**
     * @param l - level of this string in array structure of tree.
     *          Function calculate a needs number of spaces AFTER a value and add it to string
     * @return changed a
     */
    public String addSpaceAfter(int l) {
        StringBuilder s = new StringBuilder("");
        int n = (int) Math.pow(2, height(root()) - l);
        for (int i = 0; i < n; i++) {
            s.append(" ");
        }
        return s.toString();
    }

    /**
     * init ArrayList what will contain Strings of each tree level
     */
    public void setTreeLevelStruct() {
        this.treeLevelStruct = new ArrayList<>(height(root()));
    }

    /**
     * @param a - string with values of interested level
     * @return String array with separate values.
     */
    public String[] levelSpliter(String a) {
        String d = a;
        while (d.contains("  ")) {
            d = d.replaceAll("  ", " ");
        }
        String[] arr = d.split(" ");

        List<String> array = new LinkedList<>();
        Collections.addAll(array, arr);
        for (int i = 0; i < array.size(); i++) {
            if (array.get(i).equals("")) {
                array.remove(i);
                i--;
            }
        }

        arr = new String[array.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = array.get(i);
        }

        return arr;
    }

    /**
     * method to full filling treeLevelStruct
     */
    public void treeArrayFilling() {
        setTreeLevelStruct();

        treeLevelStruct.add(0, addSpaceBefore(0) + root().getElement().toString());

        for (int i = 0; i < height(root()) - 1; i++) {
            nextLevelFilling(i);
        }
    }

    /**
     * @param l - processed level already comprising values
     */
    public void nextLevelFilling(int l) {
        String[] existingLevel = levelSpliter(treeLevelStruct.get(l));

        treeLevelStruct.add("");
        for (String a : existingLevel) {
            treeLevelStruct.set(l + 1, treeLevelStruct.get(l + 1) + generateNodeChildValuesString(a, l));
        }
    }

    /**
     * Detect type of root().getElement()
     * and convert a to that type.
     *
     * @param a - value in String format
     * @return a in correct type
     */
    public E valueTypeDetector(String a) {
        if (root().getElement() instanceof Byte) {
            return (E) Byte.valueOf(a);
        } else if (root().getElement() instanceof Short) {
            return (E) Short.valueOf(a);
        } else if (root().getElement() instanceof Character) {
            return (E) Character.valueOf(a.charAt(0));
        } else if (root().getElement() instanceof Integer) {
            return (E) Integer.valueOf(a);
        } else if (root().getElement() instanceof Long) {
            return (E) Long.valueOf(a);
        } else if (root().getElement() instanceof Float) {
            return (E) Float.valueOf(a);
        } else if (root().getElement() instanceof Double) {
            return (E) Double.valueOf(a);
        } else if (root().getElement() instanceof Boolean) {
            return (E) Boolean.valueOf(a);
        } else if (root().getElement() instanceof String) {
            return (E) String.valueOf(a);
        }
        return null;
    }

    public String generateNodeChildValuesString(String a, int l) {
        StringBuilder str = new StringBuilder("");
        Node<E> node;
        if (Objects.equals(a, "-")) {
            node = null;
        } else {
            node = treeSearch(root(), valueTypeDetector(a));
        }

        str.append(addSpaceBefore(l + 1));//add space before
        try {
            str.append(validate(node).getLeft().getElement().toString());//add 1st val
        } catch (NullPointerException e) {
            str.append('-');
        }
        str.append(addSpaceAfter(l + 1));//add space before

        str.append(addSpaceBefore(l + 1));//add space before
        try {
            str.append(validate(node).getRight().getElement().toString());//add 2nd val
        } catch (NullPointerException e) {
            str.append('-');
        }
        str.append(addSpaceAfter(l + 1));//add space before

        return str.toString();
    }

    /**
     * Printing tree structure
     */
    public void treePrint() {
        treeArrayFilling();

        treeLevelStruct.forEach(System.out::println);
    }
}