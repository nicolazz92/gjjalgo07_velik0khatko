package main.java.com.getjavajob.training.algo07.velikohatkon.lesson09;

import main.java.com.getjavajob.training.algo07.util.Assert;

import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

/**
 * Created by ??????? on 03.02.2016.
 * on 12:11
 */
public class NavigableSetTest extends SortedSetTest {

    private static NavigableSet<Integer> set = new TreeSet<>();

    private static void setInit() {
        set.add(1);
        set.add(2);
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(6);
        set.add(7);
        set.add(8);
        set.add(9);
        set.add(10);
    }

    public static void lowerTest() {
        Assert.assertEquals(set.lower(3), new Integer(2));
        Assert.assertEquals(set.lower(1), null);
    }

    public static void floorTest() {
        Assert.assertEquals(set.floor(15), new Integer(10));
        Assert.assertEquals(set.floor(5), new Integer(5));
        Assert.assertEquals(set.floor(-10), null);
        try {
            Assert.assertEquals(set.floor(null), null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void ceilingTest() {
        Assert.assertEquals(set.ceiling(-10), new Integer(1));
        Assert.assertEquals(set.ceiling(11), null);
        try {
            Assert.assertEquals(set.ceiling(null), null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void higherTest() {
        Assert.assertEquals(set.higher(-9), new Integer(1));
        Assert.assertEquals(set.higher(9), new Integer(10));
        try {
            Assert.assertEquals(set.higher(null), null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void pollFirstTest() {
        TreeSet<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);

        Assert.assertEquals(set.pollFirst(), new Integer(1));
        Assert.assertEquals(set.pollFirst(), new Integer(2));
        Assert.assertEquals(set.pollFirst(), null);
    }

    public static void pollLastTest() {
        TreeSet<Integer> set = new TreeSet<>();
        set.add(1);
        set.add(2);

        Assert.assertEquals(set.pollLast(), new Integer(2));
        Assert.assertEquals(set.pollLast(), new Integer(1));
        Assert.assertEquals(set.pollLast(), null);
    }

    public static void iteratorSet() {
        Iterator<Integer> iter = set.iterator();

        String s = "";
        while (iter.hasNext()) {
            s += iter.next();
        }
        Assert.assertEquals(s, "12345678910");
    }

    public static void main(String[] args) {
        setInit();

        lowerTest();
        floorTest();
        ceilingTest();
        higherTest();
        pollFirstTest();
        pollLastTest();
        iteratorSet();
    }
}
