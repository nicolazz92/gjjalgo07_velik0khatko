package main.java.com.getjavajob.training.algo07.velikohatkon.lesson09;

import main.java.com.getjavajob.training.algo07.util.Assert;
import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;

import java.util.Iterator;
import java.util.Scanner;

/**
 * Created by nicolas on 08.02.16.
 * at 16:38
 */
public class RedBlackTreeTest {
    public static void addTest() {
        RedBlackTree<Integer> d = new RedBlackTree<>();
        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);
        d.add(75);
        d.add(125);
        d.add(175);

        Iterator<Node<Integer>> iterPre = d.iterator("pre");
        Iterator<Node<Integer>> iterIn = d.iterator("in");
        Iterator<Node<Integer>> iterPost = d.iterator("post");
        Iterator<Node<Integer>> iterWidth = d.iterator("width");
        try {
            d.iterator("bag");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "wrong execNum value");
        }

        String a = "";
        while (iterPre.hasNext()) {
            a += iterPre.next().getElement() + " ";
        }
        Assert.assertEquals(a, "100 50 25 75 150 125 175 ");

        a = "";
        while (iterIn.hasNext()) {
            a += iterIn.next().getElement() + " ";
        }
        Assert.assertEquals(a, "25 50 75 100 125 150 175 ");

        a = "";
        while (iterPost.hasNext()) {
            a += iterPost.next().getElement() + " ";
        }
        Assert.assertEquals(a, "25 75 50 125 175 150 100 ");

        a = "";
        while (iterWidth.hasNext()) {
            a += iterWidth.next().getElement() + " ";
        }
        Assert.assertEquals(a, "100 50 150 25 75 125 175 ");

        System.out.println();

    }

    public static void removeTest() {
        RedBlackTree<Integer> d = new RedBlackTree<>();

        d.add(1);
        d.add(2);
        d.add(3);
        d.add(4);
        d.add(5);
        d.add(6);
        d.add(7);
        d.add(8);
        d.add(9);
        d.treePrint();
        System.out.println();

        Scanner scanner = new Scanner(System.in);

        int scan = 0;
        while (scan != 111) {
            System.out.print("Remove elem: ");
            scan = scanner.nextInt();
            if (scan == 111) {
                continue;
            }
            d.removeElem(scan, d.getRoot());
            d.treePrint();
        }
    }

    public static void balanceAddTest() {
        RedBlackTree<Integer> d = new RedBlackTree<>();

        Scanner scanner = new Scanner(System.in);

        int scan = 0;
        while (scan != 111) {
            System.out.print("Add elem: ");
            scan = scanner.nextInt();
            d.add(scan);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        addTest();
        removeTest();
        balanceAddTest();
    }

}
