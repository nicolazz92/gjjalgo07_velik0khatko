package main.java.com.getjavajob.training.algo07.velikohatkon.lesson01;

/**
 * Created by ������� on 11.08.2015.
 * at 14:12.
 */
public class Task06 {
    public static int a(int n) {
        return 1 << n;
    }

    public static int b(int n, int m) {
        return 1 << n | 1 << m;
    }

    public static int c(int a, int n) {
        int mask = -1;
        mask <<= n;
        return a & mask;
    }

    public static int d(int a, int n) {
        int mask = 1 << n - 1;
        return a | mask;
    }

    public static int e(int a, int n) {
        int mask = 1 << n - 1;
        return a ^ mask;
    }

    public static int f(int a, int n) {
        int mask = 1 << n - 1;
        mask = ~mask;
        return a & mask;
    }

    public static int g(int a, int n) {
        int mask = -1 << n - 1;
        mask = ~mask;
        return a & mask;
    }

    public static int h(int a, int n) {
        a = a >> n - 1;
        return a & 1;
    }

    public static int i(int a) {
        System.out.println();
        int res = 0;
        for (int i = 7; i >= 0; i--) {
            System.out.print((a >>> i) & 1);
            res *= 10;
            res += (a >>> i) & 1;
        }
        return res;
    }

}
