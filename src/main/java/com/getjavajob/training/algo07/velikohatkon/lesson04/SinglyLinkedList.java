package main.java.com.getjavajob.training.algo07.velikohatkon.lesson04;

import java.util.*;

/**
 * Created by nicolas on 06.10.15.
 * at 17:20
 */

public class SinglyLinkedList<V> implements List<V> {

    public class Node<K> {
        public Node<K> next;
        public K val;
    }

    private Node<V> head = new Node<>();
    private Node<V> tail = new Node<>();
    private int size = 0;

    public void setSize(int size) {
        this.size = size;
    }

    public Node<V> getHead() {
        return head;
    }

    public static class ListIteratorImpl<V> implements ListIterator {

        private SinglyLinkedList<V> d = new SinglyLinkedList<>();
        int listIndex = 0;

        @Override
        public boolean hasNext() {
            try {
                d.get(listIndex + 1);
            } catch (ArrayIndexOutOfBoundsException e) {
                return false;
            }
            return true;
        }

        @Override
        public Object next() {
            return d.get(++listIndex);
        }

        @Override
        public boolean hasPrevious() {
            return listIndex - 1 >= 0;
        }

        @Override
        public Object previous() {
            return d.get(--listIndex);
        }

        @Override
        public int nextIndex() {
            return ++listIndex;
        }

        @Override
        public int previousIndex() {
            return --listIndex;
        }

        @Override
        public void remove() {
            d.remove(listIndex);
        }

        @Override
        public void set(Object o) {
            d.set(listIndex, (V) o);
        }

        @Override
        public void add(Object o) {
            d.add((V) o);
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head.val == null;
    }

    public boolean contains(Object o) {
        if (isEmpty()) {
            return false;
        }

        if (head.val.equals(o)) {
            return true;
        }

        Node<V> t = head;
        while (t.next != null) {
            t = t.next;
            if (t.val.equals(o)) {
                return true;
            }
        }

        return false;
    }

    public Iterator<V> iterator() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Iterator iterator()");
    }

    public Object[] toArray() {
        if (isEmpty()) {
            return null;
        }
        Object[] mas = new Object[size];
        mas[0] = head.val;
        Node<V> t = head;
        int i = 1;
        while (t.next != null && i != size) {
            t = t.next;
            mas[i] = t.val;
            i++;
        }
        return mas;
    }

    public boolean add(V o) {
        Node<V> a = new Node<>();
        a.val = o;
        if (isEmpty()) {
            head = a;
            tail = a;
        } else {
            tail.next = a;
            tail = a;
        }
        size++;
        return true;
    }

    public boolean remove(Object o) {
        if (isEmpty()) {
            return false;
        }
        if (head == tail && tail.val.equals(o)) {
            head = null;
            tail = null;
            size--;
            return true;
        }
        if (head.val.equals(o)) {
            head = head.next;
            size--;
            return true;
        }

        Node<V> t = head;
        if (t.next != null && t.next.val.equals(o)) {
            t.next = t.next.next;
            size--;
            return true;
        }
        return false;
    }

    @Override
    public boolean addAll(Collection collection) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("boolean addAll(Collection collection)");
    }

    @Override
    public boolean addAll(int i, Collection collection) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("boolean addAll(int i, Collection collection)");
    }

    @Override
    public void clear() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("void clear()");
    }

    @Override
    public V get(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("get(int i): i is too big");
        }
        Node<V> t = head;
        for (int k = 0; k < i; k++) {
            t = t.next;
        }
        return t.val;
    }

    @Override
    public V set(int i, V o) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("set(int i, Object o): i is too big");
        }
        Node<V> t = head;
        for (int k = 0; k < i; k++) {
            t = t.next;
        }
        Node<V> res = t;
        t.val = o;
        return res.val;
    }

    @Override
    public void add(int i, V o) throws ArrayIndexOutOfBoundsException {
        if (i > size) {
            throw new ArrayIndexOutOfBoundsException("void add(int i, Object o): i is too big");
        }
        Node<V> a = new Node<>();
        a.val = o;
        if (i == 0) {
            a.next = head;
            head = a;
        }
        if (i > 0 && i < size) {
            Node<V> t = head;
            for (int k = 0; k < i - 1; k++) {
                t = t.next;
            }
            a.next = t.next;
            t.next = a;
        }
        if (i == size) {
            if (size != 0) {
                tail.next = a;
            }
            tail = a;
        }
        size++;
    }

    public V remove(int i) {
        Node<V> t = head;
        Node<V> res;
        if (i == 0) {
            res = head;
            head = head.next;
            size--;
            return res.val;
        }

        for (int k = 0; k < i - 1; k++) {
            t = t.next;
        }
        res = t.next;
        t.next = t.next.next;
        if (t.next == null) {
            tail = t;
        }
        size--;
        return res.val;
    }

    public int indexOf(Object o) throws NoSuchElementException {
        if (isEmpty()) {
            throw new NoSuchElementException("list is empty");
        }
        if (head.val.equals(o)) {
            return 0;
        }
        int count = 0;
        Node<V> t = head;
        while (!t.val.equals(o) && t.next != null) {
            t = t.next;
            count++;
            if (t.val.equals(o)) {
                return count;
            }
        }
        if (count == size - 1) {
            throw new NoSuchElementException("element has not being found");
        }
        return count;
    }

    public Node<V> getElem(int num) {
        Node<V> result;
        result = head;
        for (int i = 0; i < num; i++) {
            result = result.next;
        }
        return result;
    }

    public boolean swap(int sw1, int sw2) {

        if (sw2 < sw1) {//select by order
            sw2 = sw2 + sw1;
            sw1 = sw2 - sw1;
            sw2 = sw2 - sw1;
        }

        Node<V> tPrev = getElem(sw1 - 1);
        Node<V> t = getElem(sw1);//first elem
        Node<V> tNext = getElem(sw1 + 1);

        Node<V> aPrev = getElem(sw2 - 1);
        Node<V> a = getElem(sw2);//second elem
        Node<V> aNext = getElem(sw2 + 1);

        if (sw1 == 0 && sw2 == 1) {
            a.next = t;
            t.next = aNext;
            head = a;
            return true;
        }

        if (sw1 == 0) {
            aPrev.next = t;
            t.next = aNext;
            a.next = tNext;
            head = a;
            return true;
        }

        if (sw2 - sw1 == 1) {
            tPrev.next = a;
            t.next = aNext;
            a.next = t;
            return true;
        }

        if (sw2 - sw1 > 1) {
            tPrev.next = a;
            a.next = tNext;
            aPrev.next = t;
            t.next = aNext;
            return true;
        }
        return true;
    }

    public boolean reverse() {
        Node<V> before = null;
        Node<V> buf = head;
        while (buf != null) {
            Node<V> next = buf.next;
            buf.next = before;
            before = buf;
            buf = next;
        }
        head = before;
        return true;
    }

    public String print() {
        String res = "";
        if (isEmpty()) {
            System.out.println(res);
            return res;
        }
        for (Object a : toArray()) {
            res += a + " ";
            System.out.print(a + " ");
        }
        System.out.println();
        return res;
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException("int lastIndexOf(Object o)");
    }

    @Override
    public ListIterator<V> listIterator() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("ListIterator listIterator()");
    }

    @Override
    public ListIterator<V> listIterator(int i) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("ListIterator listIterator(int i)");
    }

    @Override
    public List<V> subList(int i, int i1) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("List subList(int i, int i1)");
    }

    @Override
    public boolean retainAll(Collection collection) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("boolean retainAll(Collection collection)");
    }

    @Override
    public boolean removeAll(Collection collection) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("boolean removeAll(Collection collection)");
    }

    @Override
    public boolean containsAll(Collection collection) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("boolean containsAll(Collection collection)");
    }

    @Override
    public Object[] toArray(Object[] objects) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("Object[] toArray(Object[] objects)");
    }
}