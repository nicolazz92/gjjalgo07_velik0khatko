package main.java.com.getjavajob.training.algo07.velikohatkon.lesson03;

import java.util.ConcurrentModificationException;

import static java.lang.System.arraycopy;

/**
 * Created by nicolas on 29.08.15.
 * at 18:03
 */
public class DynamicArray {
    private Object[] mass;
    private int size;

    public DynamicArray() {
        int BEGIN_SIZE = 10;
        mass = new Object[BEGIN_SIZE];
        size = 0;
    }

    DynamicArray(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException("DynamicArray(int i): i < 0");
        }
        mass = new Object[i];
        size = 0;
    }

    public static class ListIterator implements java.util.ListIterator {

        private int iterIndex = 0;
        DynamicArray d;

        public ListIterator(int i) {
            d = new DynamicArray(i);
        }

        @Override
        public boolean hasNext() throws ConcurrentModificationException {
            return (iterIndex < d.size() - 1);
        }

        @Override
        public Object next() throws ConcurrentModificationException {
            iterIndex++;
            return d.mass[iterIndex];
        }

        @Override
        public boolean hasPrevious() throws ConcurrentModificationException {
            return (iterIndex > 0);
        }

        @Override
        public Object previous() throws ConcurrentModificationException {
            iterIndex--;
            return d.mass[iterIndex];
        }

        @Override
        public int nextIndex() throws ConcurrentModificationException {
            return iterIndex++;
        }

        @Override
        public int previousIndex() throws ConcurrentModificationException {
            return iterIndex--;
        }

        @Override
        public void remove() throws ConcurrentModificationException {
            d.rmObject(iterIndex);
        }

        @Override
        public void set(Object e) throws ConcurrentModificationException {
            d.set(iterIndex, e);
        }

        @Override
        public void add(Object e) throws ConcurrentModificationException {
            d.add(iterIndex, e);
            iterIndex++;
        }

    }

    boolean add(Object e) {
        growIfFull();
        mass[size] = e;
        size++;
        return true;
    }

    void add(int i, Object e) throws ArrayIndexOutOfBoundsException {
        if (i > size) {
            throw new ArrayIndexOutOfBoundsException("add(int i, Object e): 'i' is too big");
        }
        growIfFull();

        arraycopy(mass, i, mass, i + 1, size - i);

        mass[i] = e;
        size++;
    }

    boolean rmObject(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0 || i > size - 1) throw new ArrayIndexOutOfBoundsException("rmObject(int i): 'i' is wrong");
        arraycopy(mass, i + 1, mass, i, size - 1 - i);
        mass[size - 1] = null;
        size--;
        return true;
    }

    boolean remove(Object e) {
        int i = searchObject(e);
        if (i == -1) {
            return false;
        }
        rmObject(i);
        return true;
    }

    Object remove(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("remove(int i): 'i' is too big");
        }
        Object r = mass[i];
        rmObject(i);
        return r;
    }

    boolean growIfFull() {
        if (size < mass.length - 1) {
            return false;
        }

        Object[] bigger = new Object[(int) (mass.length * 1.5)];
        arraycopy(mass, 0, bigger, 0, mass.length);
        mass = bigger;
        return true;
    }

    Object set(int i, Object e) throws ArrayIndexOutOfBoundsException {
        if (i >= mass.length) {
            throw new ArrayIndexOutOfBoundsException("set(int i, Object e): 'i' is too big");
        }
        Object r = mass[i];
        mass[i] = e;
        return r;
    }

    Object get(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("get(int i): 'i' is too big");
        }
        return mass[i];
    }

    int searchObject(Object elem) {
        for (int i = 0; i < size; i++) {
            if (elem == null && mass[i] == null) {
                return i;
            }
            if (mass[i].equals(elem)) {
                return i;
            }
        }
        return -1;
    }

    int size() {
        return size;
    }

    int indexOf(Object e) throws ArrayIndexOutOfBoundsException {
        int i = searchObject(e);
        if (i == -1) {
            throw new ArrayIndexOutOfBoundsException("indexOf(Object e): There is no this element");
        }
        return i;
    }

    boolean contains(Object e) {
        int i = searchObject(e);
        return i == -1;
    }

    Object[] toArray() {
        return mass;
    }
}
