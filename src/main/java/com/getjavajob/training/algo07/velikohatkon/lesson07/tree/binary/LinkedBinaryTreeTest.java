package main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.binary;

import main.java.com.getjavajob.training.algo07.util.Assert;
import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;

import java.util.Iterator;

/**
 * Created by Николай on 14.12.2015.
 * on 11:18
 */
public class LinkedBinaryTreeTest {

    public static void addTest() {
        LinkedBinaryTree<Integer> d = new LinkedBinaryTree<>();

        try {
            d.add(d.root(), 25);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "parent is null");
        }

        d.add(18);
        d.add(12);
        d.add(23);
        d.add(32);

        try {
            d.add(d.right(d.root()), 25);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "node already has a right child");
        }

        try {
            d.add(d.root(), 6);
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "node already has a left child");
        }

        Assert.assertEquals(d.add(d.left(d.root()), 2).getElement(), new Integer(2));
        Assert.assertEquals(d.add(d.left(d.left(d.root())), 3).getElement(), new Integer(3));
    }

    public static void removeTest() {
        LinkedBinaryTree<Integer> d = new LinkedBinaryTree<>();

        d.add(15);
        d.add(6);
        d.add(18);
        d.add(3);
        d.add(7);
        d.add(17);
        d.add(20);
        d.add(2);
        d.add(4);
        d.add(13);
        d.add(9);

        Assert.assertEquals(d.removeElem(20, d.root()), new Integer(20));
        Assert.assertEquals(d.removeElem(18, d.root()), new Integer(18));
        Assert.assertEquals(d.removeElem(7, d.root()), new Integer(7));
        Assert.assertEquals(d.removeElem(6, d.root()), new Integer(6));
        Assert.assertEquals(d.removeElem(9, d.root()), new Integer(9));
        Assert.assertEquals(d.removeElem(13, d.root()), new Integer(13));

        System.out.println("");
    }

    public static void iterTest() {
        LinkedBinaryTree<Integer> d = new LinkedBinaryTree<>();
        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);
        d.add(75);
        d.add(125);
        d.add(175);

        Iterator<Node<Integer>> iterPre = d.iterator("pre");
        Iterator<Node<Integer>> iterIn = d.iterator("in");
        Iterator<Node<Integer>> iterPost = d.iterator("post");
        Iterator<Node<Integer>> iterWidth = d.iterator("width");
        try {
            d.iterator("bag");
        } catch (IllegalArgumentException e) {
            Assert.assertEquals(e.getMessage(), "wrong execNum value");
        }

        String a = "";
        while (iterPre.hasNext()) {
            a += iterPre.next().getElement() + " ";
        }
        Assert.assertEquals(a, "100 50 25 75 150 125 175 ");

        a = "";
        while (iterIn.hasNext()) {
            a += iterIn.next().getElement() + " ";
        }
        Assert.assertEquals(a, "25 50 75 100 125 150 175 ");

        a = "";
        while (iterPost.hasNext()) {
            a += iterPost.next().getElement() + " ";
        }
        Assert.assertEquals(a, "25 75 50 125 175 150 100 ");

        a = "";
        while (iterWidth.hasNext()) {
            a += iterWidth.next().getElement() + " ";
        }
        Assert.assertEquals(a, "100 50 150 25 75 125 175 ");

        System.out.println();
    }

    public static void isGetRootTest() {
        LinkedBinaryTree<Integer> d = new LinkedBinaryTree<>();
        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);
        d.add(75);
        d.add(125);
        d.add(175);

        Assert.assertEquals(d.isRoot(d.getRoot()), true);
        Assert.assertEquals(d.isRoot(d.validate(d.getRoot()).getRight()), false);
    }

    public static void childrenTest() {
        LinkedBinaryTree<Integer> d = new LinkedBinaryTree<>();
        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);

        Iterator<Node<Integer>> iterRoot = d.children(d.getRoot()).iterator();
        String a = "";
        while (iterRoot.hasNext()) {
            a += iterRoot.next().getElement() + " ";
        }
        Assert.assertEquals(a, "50 150 ");

        Iterator<Node<Integer>> iterLeft = d.children(d.validate(d.getRoot()).getLeft()).iterator();
        a = "";
        while (iterLeft.hasNext()) {
            a += iterLeft.next().getElement() + " ";
        }
        Assert.assertEquals(a, "25 ");

        Iterator<Node<Integer>> iterRight = d.children(d.validate(d.getRoot()).getRight()).iterator();
        a = "";
        while (iterRight.hasNext()) {
            a += iterRight.next().getElement() + " ";
        }
        Assert.assertEquals(a, "");
    }

    public static void internalExternalTest() {
        LinkedBinaryTree<Integer> d = new LinkedBinaryTree<>();
        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);

        Assert.assertEquals(d.isInternal(d.root()), true);
        Assert.assertEquals(d.isInternal(d.validate(d.getRoot()).getLeft()), true);
        Assert.assertEquals(d.isInternal(d.validate(d.getRoot()).getRight()), false);

        Assert.assertEquals(d.isExternal(d.root()), false);
        Assert.assertEquals(d.isExternal(d.validate(d.getRoot()).getLeft()), false);
        Assert.assertEquals(d.isExternal(d.validate(d.getRoot()).getRight()), true);
    }

    public static void main(String[] args) {
        addTest();
        removeTest();
        iterTest();
        isGetRootTest();
        childrenTest();
        internalExternalTest();
    }
}