package main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.binary;

import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Concrete implementation of a binary tree using a node-based, linked structure
 *
 * @param <E> element
 */
public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> {

    public LinkedBinaryTree() {
    }

    private Node<E> root = null;
    private int size = 0;

    public Node<E> getRoot() {
        return root;
    }

    public Node<E> setRoot(Node<E> node) {
        root = node;
        validate(root).setParent(null);
        return root;
    }

    public void sizePlus(){
        size++;
    }

    public boolean add(E d) {
        NodeImpl<E> elem = validate(getRoot());
        if (isEmpty()) {
            addRoot(d);
            sizePlus();
            return true;
        }
        while (elem.getElement() != null) {
            if (d.hashCode() < elem.getElement().hashCode()) {
                if (elem.getLeft() != null) {
                    elem = validate(elem.getLeft());
                    continue;
                }
                addLeft(elem, d);
            } else if (d.hashCode() > elem.getElement().hashCode()) {
                if (elem.getRight() != null) {
                    elem = validate(elem.getRight());
                    continue;
                }
                addRight(elem, d);
            } else if (d.hashCode() == elem.getElement().hashCode()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validates the node is an instance of supported {@link NodeImpl} type and casts to it
     *
     * @param n node
     * @return casted {@link NodeImpl} node
     * @throws IllegalArgumentException
     */
    public NodeImpl<E> validate(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        }
        try {
            return (NodeImpl<E>) n;
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    // update methods supported by this class

    /**
     * Places element <i>e</i> at the root of an empty tree and returns its new {@link Node}
     *
     * @param e element
     * @return created root
     * @throws IllegalStateException if tree is not empty
     */
    public Node<E> addRoot(E e) throws IllegalStateException {
        try {
            setRoot(new NodeImpl<>(e));
            return getRoot();
        } catch (IllegalArgumentException a) {
            System.out.println("wrong arg type");
            return null;
        }
    }

    @Override
    public Node<E> add(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("parent is null");
        }
        NodeImpl<E> elem = validate(n);
        if (e.hashCode() < elem.getElement().hashCode()) {//left child
            return addLeft(n, e);
        } else if (e.hashCode() > elem.getElement().hashCode()) {//right child
            return addRight(n, e);
        }

        return null;
    }

    /**
     * Creates a new left child of {@link Node} <i>n</i> storing element <i>e</i>
     *
     * @param n node
     * @param e element
     * @return created node
     * @throws IllegalArgumentException if <i>node</i> already has a left child
     */
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("parent is null");
        }
        NodeImpl<E> d = validate(n);
        if (d.getLeft() == null) {
            d.setLeft(new NodeImpl<>(e));
            validate(d.getLeft()).setParent(d);
            sizePlus();
            return d.getLeft();
        } else {
            throw new IllegalArgumentException("node already has a left child");
        }
    }

    /**
     * Creates a new right child of {@link Node} <i>n</i> storing element <i>e</i>
     *
     * @param n node
     * @param e element
     * @return created node
     * @throws IllegalArgumentException if <i>n</i> already has a right child
     */
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("parent is null");
        }
        NodeImpl<E> d = validate(n);
        if (d.getRight() == null) {
            d.setRight(new NodeImpl<>(e));
            validate(d.getRight()).setParent(d);
            sizePlus();
            return d.getRight();
        } else {
            throw new IllegalArgumentException("node already has a right child");
        }
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @param e element
     * @return replace element
     * @throws IllegalArgumentException
     */
    public E set(Node<E> n, E e) throws IllegalArgumentException {
        try {
            NodeImpl<E> d = validate(n);
            E lastVal = d.getElement();
            if (e.hashCode() > d.left.getElement().hashCode() &&
                    e.hashCode() < d.right.getElement().hashCode()) {
                validate(n).setData(e);
                return lastVal;
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException a) {
            System.out.println("wrong arg type or val");
            return null;
        }
    }

    /**
     * Replaces the element at {@link Node} <i>n</i> with <i>e</i>
     *
     * @param n node
     * @return replace element
     * @throws IllegalArgumentException
     */
    public E remove(Node<E> n) throws IllegalArgumentException {
        NodeImpl<E> d = validate(n);
        if (d.getRight() == null && d.getLeft() == null) {//list
            return treeListRemove(d);
        } else if (d.getRight() == null ^ d.getLeft() == null) {//one child
            return treeOneChildRemove(d);
        } else if (d.getRight() != null && d.getLeft() != null) {//two children
            return treeTwoChildrenRemove(d);
        }
        return null;
    }

    /**
     * @param a    - value of removING element
     * @param node - root of a removable tree
     * @return value of removED element.
     */
    public E removeElem(E a, Node<E> node) {
        NodeImpl<E> n = validate(node);
        if (a.hashCode() == n.getElement().hashCode()) {
            return remove(node);
        } else if (a.hashCode() < n.getElement().hashCode()) {
            if (n.getLeft() != null) {
                Node<E> c = n.getLeft();
                return removeElem(a, c);
            }
        } else if (a.hashCode() > n.getElement().hashCode()) {
            if (n.getRight() != null) {
                Node<E> c = n.getRight();
                return removeElem(a, c);
            }
        }
        return null;
    }

    private E treeTwoChildrenRemove(Node<E> d) {
        NodeImpl<E> a = validate(d);
        E res = a.getElement();
        NodeImpl<E> r = validate(a.right);
        while (r.left != null) {
            r = validate(r.left);
        }
        E buf = r.getElement();
        remove(r);
        a.setData(buf);
        //there is no "size--" because of it has done in removeElem(r)
        return res;
    }

    private E treeOneChildRemove(Node<E> d) {
        NodeImpl<E> a = validate(d);
        E res = a.getElement();
        if (a.right != null) {//if a has RIGHT child
            validate(a.right).setParent(validate(a.parent));
            if (validate(a.parent).right == a) {//if a is a RIGHT child
                validate(a.parent).right = a.right;
            } else if (validate(a.parent).left == a) {//if a is a LEFT child
                validate(a.parent).left = a.right;
            }
            size--;
            return res;
        } else if (a.left != null) {//if a has LEFT child
            validate(a.left).setParent(validate(a.parent));
            if (validate(a.parent).right == a) {//if a is a RIGHT child
                validate(a.parent).right = a.left;
            } else if (validate(a.parent).left == a) {//if a is a LEFT child
                validate(a.parent).left = a.left;
            }
            size--;
            return res;
        }
        return null;
    }

    private E treeListRemove(Node<E> d) {
        NodeImpl<E> a = validate(d);
        E res = d.getElement();
        if (d.getElement().hashCode() < a.getParent().getElement().hashCode()) {
            validate(a.parent).setLeft(null);
            size--;
            return res;
        } else if (d.getElement().hashCode() > a.getParent().getElement().hashCode()) {
            validate(a.parent).setRight(null);
            size--;
            return res;
        }
        return null;
    }

    // {@link Tree} and {@link BinaryTree} implementations

    @Override
    public Node<E> left(Node<E> p) throws IllegalArgumentException {
        try {
            return validate(p).getLeft();
        } catch (IllegalArgumentException e) {
            System.out.println();
            return null;
        }
    }

    @Override
    public Node<E> right(Node<E> p) throws IllegalArgumentException {
        try {
            return validate(p).getRight();
        } catch (IllegalArgumentException e) {
            System.out.println();
            return null;
        }
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public Node<E> root() {
        return root;
    }

    @Override
    public Node<E> parent(Node<E> n) throws IllegalArgumentException {
        try {
            return validate(n).getParent();
        } catch (IllegalArgumentException e) {
            System.out.println();
            return null;
        }
    }

    /**
     * @param n node
     * @return an iterable collection containing the children of node <i>node</i> (if any)
     * @throws IllegalArgumentException
     */
    @Override
    public Iterable<Node<E>> children(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("Node is null");
        }
        List<Node<E>> list = new LinkedList<>();
        NodeImpl<E> node = validate(n);
        if (node.getLeft() != null) {
            list.add(node.getLeft());
        }
        if (node.getRight() != null) {
            list.add(node.getRight());
        }
        return list;
    }

    /**
     * @param n node
     * @return the number of children of node <i>n</i>
     * @throws IllegalArgumentException if <i>n</i> instance is not of supported type
     */
    public int childrenNumber(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("Node is null");
        }
        int res = 0;
        NodeImpl<E> node = validate(n);
        if (node.getLeft() != null) {
            res++;
        }
        if (node.getRight() != null) {
            res++;
        }
        return res;
    }

    /**
     * @param n node
     * @return true if node <i>n</i> has at least one child
     * @throws IllegalArgumentException if <i>n</i> instance is not of supported type
     */
    public boolean isInternal(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("Node is null");
        }
        return childrenNumber(n) > 0;
    }

    /**
     * @param n node
     * @return true if node <i>n</i> does not have any children
     * @throws IllegalArgumentException if <i>n</i> instance is not of supported type
     */
    public boolean isExternal(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("Node is null");
        }
        return childrenNumber(n) == 0;
    }

    /**
     * @param n node
     * @return true if node <i>n</i> is the root of the tree
     * @throws IllegalArgumentException if <i>n</i> instance is not of supported type
     */
    public boolean isRoot(Node<E> n) throws IllegalArgumentException {
        try {
            validate(n);
        } catch (IllegalArgumentException e) {
            System.out.println("Node is unsupported type");
        }
        return n == root;
    }

    @Override
    public int size() {
        return size;
    }

    /**
     * @return list of values from list of pre-order traversal nodes
     */
    @Override
    public Iterator<E> iterator() {
        List<Node<E>> list = nodes("pre");
        List<E> result = list.stream().map(Node::getElement).collect(Collectors.toCollection(LinkedList::new));
        return result.iterator();
    }

    /**
     * @return list of nodes from list of chosen traversal nodes
     */
    public Iterator<Node<E>> iterator(String execNum) {
        List<Node<E>> list = nodes(execNum);
        return list.iterator();
    }

    /**
     * @return pre-order traversal nodes list by default
     */
    @Override
    public Iterable<Node<E>> nodes() {
        List<Node<E>> list = new LinkedList<>();
        preOrderTraversal(root(), list);
        return list;
    }

    /**
     * @param execNum - case a travers order type
     * @return nodes list with tree elements in cased order travers
     * @throws IllegalArgumentException if execNum != "pre", "in" or "post"
     */
    public List<Node<E>> nodes(String execNum) {
        if (!Objects.equals(execNum, "pre") &&
                !Objects.equals(execNum, "in") &&
                !Objects.equals(execNum, "post") &&
                !Objects.equals(execNum, "width")) {
            throw new IllegalArgumentException("wrong execNum value");
        }
        NodeImpl<E> n = validate(root());
        List<Node<E>> list = new LinkedList<>();

        switch (execNum) {
            case "pre":
                preOrderTraversal(n, list);
                return list;
            case "in":
                inOrderTraversal(n, list);
                return list;
            case "post":
                postOrderTraversal(n, list);
                return list;
            case "width":
                widthOrderTraversal(n, list);
                return list;
        }

        return null;
    }

    /**
     * preOrderTraversal
     *
     * @param node - root node of tree
     * @param list - container for nodes
     */
    private void preOrderTraversal(Node<E> node, List<Node<E>> list) {
        NodeImpl<E> n = validate(node);
        list.add(n);
        if (n.getLeft() != null) {
            preOrderTraversal(n.getLeft(), list);
        }
        if (n.getRight() != null) {
            preOrderTraversal(n.getRight(), list);
        }
    }

    /**
     * inOrderTraversal
     *
     * @param node - root node of tree
     * @param list - container for nodes
     */
    private void inOrderTraversal(Node<E> node, List<Node<E>> list) {
        NodeImpl<E> n = validate(node);
        if (n.getLeft() != null) {
            inOrderTraversal(n.getLeft(), list);
        }
        list.add(n);
        if (n.getRight() != null) {
            inOrderTraversal(n.getRight(), list);
        }
    }

    /**
     * postOrderTraversal
     *
     * @param node - root node of tree
     * @param list - container for nodes
     */
    private void postOrderTraversal(Node<E> node, List<Node<E>> list) {
        NodeImpl<E> n = validate(node);
        if (n.getLeft() != null) {
            postOrderTraversal(n.getLeft(), list);
        }
        if (n.getRight() != null) {
            postOrderTraversal(n.getRight(), list);
        }
        list.add(n);
    }

    /**
     * Traversal in width
     */
    private void widthOrderTraversal(Node<E> node, List<Node<E>> list) {
        int h = height(node);
        int i;
        for (i = 1; i <= h; i++) {
            listingGivenLevel(node, i, list);
        }
    }

    /**
     * @param root  - root elem of tree
     * @param level - level if listing tree
     * @param list  - result iterable list
     */
    private void listingGivenLevel(Node<E> root, int level, List<Node<E>> list) {
        if (root == null)
            return;
        if (level == 1) {
            list.add(root);
        } else if (level > 1) {
            listingGivenLevel(validate(root).getLeft(), level - 1, list);
            listingGivenLevel(validate(root).getRight(), level - 1, list);
        }
    }

    /**
     * @param node - root of counting tree
     * @return height of tree
     */
    public int height(Node<E> node) {
        if (node == null) {
            return 0;
        } else {
            /* compute  height of each subtree */
            int lheight = height(validate(node).left);
            int rheight = height(validate(node).right);

            /* use the larger one */
            if (lheight > rheight) {
                return (lheight + 1);
            } else {
                return (rheight + 1);
            }
        }
    }

    public static class NodeImpl<E> implements Node<E> {
        private E data;
        private Node<E> parent;
        private Node<E> right;
        private Node<E> left;

        public NodeImpl(Node<E> a) {
            LinkedBinaryTree<E> d = new LinkedBinaryTree<>();
            this.data = d.validate(a).getElement();
            this.parent = d.validate(a).getParent();
            this.right = d.validate(a).getRight();
            this.left = d.validate(a).getLeft();
        }

        public NodeImpl(E data) {
            this.data = data;
        }

        public void setData(E data) {
            this.data = data;
        }

        public void setParent(NodeImpl<E> parent) {
            this.parent = parent;
        }

        public void setRight(NodeImpl<E> right) {
            this.right = right;
        }

        public void setLeft(NodeImpl<E> left) {
            this.left = left;
        }

        public Node<E> getParent() {
            return parent;
        }

        public Node<E> getRight() {
            return right;
        }

        public Node<E> getLeft() {
            return left;
        }

        @Override
        public E getElement() {
            return data;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            NodeImpl<?> node = (NodeImpl<?>) o;

            return data != null ? data.equals(node.data) : node.data == null;

        }

        @Override
        public int hashCode() {
            return data != null ? data.hashCode() : 0;
        }
    }

}