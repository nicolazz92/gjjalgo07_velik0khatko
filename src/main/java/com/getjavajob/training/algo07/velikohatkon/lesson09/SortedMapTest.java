package main.java.com.getjavajob.training.algo07.velikohatkon.lesson09;

import main.java.com.getjavajob.training.algo07.util.Assert;

import java.util.*;

/**
 * Created by ??????? on 03.02.2016.
 * on 17:38
 */
public class SortedMapTest {

    private static SortedMap<Integer, String> map = new TreeMap<>();
    private static SortedMap<Integer, String> sorMap;
    private static List<String> list;

    private static void mapInit() {
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
        map.put(8, "eight");
        map.put(9, "nine");
        map.put(10, "ten");
    }

    public static void comparatorTest() {
        Comparator<? super Integer> comp = map.comparator();
        Assert.assertEquals(comp, null);
    }

    public static void subMapTest() {
        sorMap = map.subMap(2, 5);
        list = new ArrayList<>(sorMap.values());
        Assert.assertEquals(list.toString(), "[two, three, four]");
    }

    public static void headMapTest() {
        sorMap = map.headMap(5);
        list = new ArrayList<>(sorMap.values());
        Assert.assertEquals(list.toString(), "[one, two, three, four]");
    }

    public static void tailMapTest() {
        sorMap = map.tailMap(5);
        list = new ArrayList<>(sorMap.values());
        Assert.assertEquals(list.toString(), "[five, six, seven, eight, nine, ten]");
    }

    public static void firstTest() {
        Object first = map.firstKey();
        Assert.assertEquals(first, 1);
    }

    public static void lastTest() {
        Object first = map.lastKey();
        Assert.assertEquals(first, 10);
    }

    public static void keySetTest() {
        Set<Integer> set = map.keySet();
        Assert.assertEquals(set.toString(), "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]");
    }

    public static void valuesTest() {
        Collection<String> collection = map.values();
        Assert.assertEquals(collection.toString(), "[one, two, three, four, five, six, seven, eight, nine, ten]");
    }

    public static void entrySetTest() {
        Set<Map.Entry<Integer, String>> setMap = map.entrySet();
        Iterator<Map.Entry<Integer, String>> iter = setMap.iterator();
        String s = "";
        while (iter.hasNext()) {
            s += iter.next() + " ";
        }
        Assert.assertEquals(s, "1=one 2=two 3=three 4=four 5=five 6=six 7=seven 8=eight 9=nine 10=ten ");
    }

    public static void main(String[] args) {
        mapInit();

        comparatorTest();
        subMapTest();
        headMapTest();
        tailMapTest();
        firstTest();
        lastTest();
        keySetTest();
        valuesTest();
        entrySetTest();
    }
}
