package main.java.com.getjavajob.training.algo07.velikohatkon.lesson10;

/**
 * Created by Николай on 29.02.2016
 * at 16:40
 */
public class InsertionSort<E> {

    public void sorting(E[] arr) {
        for (int i = 1; i < arr.length; i++) {
            E x = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j].hashCode() > x.hashCode()) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = x;
        }
    }

}