package main.java.com.getjavajob.training.algo07.velikohatkon.lesson09;

import java.util.NavigableMap;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * Created by ??????? on 04.02.2016.
 * on 16:00
 */
public class CityLookUpStorage {

    private NavigableMap<String, String> storage = new TreeMap<>();

    public void add(String city) {
        storage.put(city.toLowerCase(), city);
    }

    public void printStorage() {
        storage.values().forEach(System.out::println);
    }

    public void printStorage(SortedMap<String, String> result) {
        result.values().forEach(System.out::println);
    }

    public SortedMap<String, String> searchBySub(String subString) {
        String subFrom = subString.toLowerCase();
        String subTo = subFrom.substring(0, subFrom.length() - 1) + (char) (subFrom.charAt(subFrom.length() - 1) + 1);
        return storage.subMap(subFrom, subTo);
    }

    public void search(String question) {
        printStorage(searchBySub(question));
    }

}
