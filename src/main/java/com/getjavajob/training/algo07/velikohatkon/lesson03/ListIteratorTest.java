package main.java.com.getjavajob.training.algo07.velikohatkon.lesson03;

/**
 * Created by nicolas on 14.09.15.
 * at 18:08
 */
public class ListIteratorTest {

    public static void hasNextPreviousTest() {
        DynamicArray.ListIterator listIterator = new DynamicArray.ListIterator(3);
        for (int i = 0; i < 10; i++) {
            listIterator.add(i);
        }
        while (listIterator.hasPrevious()) {
            System.out.print(listIterator.previous() + " ");
        }
        System.out.println();
        while (listIterator.hasNext()) {
            System.out.print(listIterator.next() + " ");
        }
        System.out.println();
        System.out.println("next index is " + listIterator.nextIndex() + ", previous index is " + listIterator.previousIndex());
        while (listIterator.hasPrevious()) {
            listIterator.previous();
        }
        listIterator.set(15);
        while (listIterator.hasNext()) {
            listIterator.remove();
        }
        System.out.println("next index is " + listIterator.nextIndex() + ", previous index is " + listIterator.previousIndex());
    }

    public static void main(String[] args) {
        hasNextPreviousTest();
    }
}
