package main.java.com.getjavajob.training.algo07.velikohatkon.lesson09;

import main.java.com.getjavajob.training.algo07.util.Assert;

import java.util.*;

/**
 * Created by ??????? on 03.02.2016.
 * on 22:01
 */
public class NavigableMapTest {

    private static NavigableMap<Integer, String> map = new TreeMap<>();
    private static NavigableMap<Integer, String> navMap;
    private static List<String> list;

    private static void mapInit() {
        map.put(1, "one");
        map.put(2, "two");
        map.put(3, "three");
        map.put(4, "four");
        map.put(5, "five");
        map.put(6, "six");
        map.put(7, "seven");
        map.put(8, "eight");
        map.put(9, "nine");
        map.put(10, "ten");
    }

    public static void lowerEntryTest() {
        Map.Entry<Integer, String> m = map.lowerEntry(5);
        Assert.assertEquals(m.toString(), "4=four");

        m = map.lowerEntry(1);
        Assert.assertEquals(m, null);

        try {
            map.lowerEntry(null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void lowerKeyTest() {
        Integer key = map.lowerKey(5);
        Assert.assertEquals(key, new Integer(4));

        key = map.lowerKey(1);
        Assert.assertEquals(key, null);

        try {
            map.lowerKey(null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void floorEntryTest() {
        Map.Entry<Integer, String> m = map.floorEntry(5);
        Assert.assertEquals(m.toString(), "5=five");

        m = map.floorEntry(11);
        Assert.assertEquals(m, null);

        try {
            map.floorEntry(null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void floorKeyTest() {
        Integer key = map.floorKey(5);
        Assert.assertEquals(key, new Integer(5));

        key = map.floorKey(0);
        Assert.assertEquals(key, null);

        try {
            map.floorKey(null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void ceilingEntryTest() {
        Map.Entry<Integer, String> m = map.ceilingEntry(-1);
        Assert.assertEquals(m.toString(), "1=one");

        m = map.ceilingEntry(5);
        Assert.assertEquals(m.toString(), "5=five");

        m = map.ceilingEntry(11);
        Assert.assertEquals(m, null);

        try {
            map.ceilingEntry(null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void ceilingKeyTest() {
        Integer key = map.ceilingKey(-1);
        Assert.assertEquals(key, new Integer(1));

        key = map.ceilingKey(5);
        Assert.assertEquals(key, new Integer(5));

        key = map.ceilingKey(11);
        Assert.assertEquals(key, null);

        try {
            map.ceilingKey(null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void higherEntryTest() {
        Map.Entry<Integer, String> m = map.higherEntry(-1);
        Assert.assertEquals(m.toString(), "1=one");

        m = map.higherEntry(5);
        Assert.assertEquals(m.toString(), "6=six");

        m = map.higherEntry(11);
        Assert.assertEquals(m, null);

        try {
            map.higherEntry(null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void higherKeyTest() {
        Integer key = map.higherKey(-1);
        Assert.assertEquals(key, new Integer(1));

        key = map.higherKey(5);
        Assert.assertEquals(key, new Integer(6));

        key = map.higherKey(11);
        Assert.assertEquals(key, null);

        try {
            map.higherKey(null);
        } catch (NullPointerException e) {
            System.out.println("Error: " + e.getMessage());
        }
    }

    public static void firstEntryTest() {
        Map.Entry<Integer, String> m = map.firstEntry();
        Assert.assertEquals(m.toString(), "1=one");

        Assert.assertEquals(new TreeMap<Integer, String>().firstEntry(), null);
    }

    public static void lastEntryTest() {
        Map.Entry<Integer, String> m = map.lastEntry();
        Assert.assertEquals(m.toString(), "10=ten");

        Assert.assertEquals(new TreeMap<Integer, String>().lastEntry(), null);
    }

    public static void pollFirstEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        Map.Entry<Integer, String> m = map.pollFirstEntry();
        Assert.assertEquals(m.toString(), "1=one");

        m = map.pollFirstEntry();
        Assert.assertEquals(m.toString(), "2=two");

        m = map.pollFirstEntry();
        Assert.assertEquals(m, null);
    }

    public static void pollLastEntryTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        Map.Entry<Integer, String> m = map.pollLastEntry();
        Assert.assertEquals(m.toString(), "2=two");

        m = map.pollLastEntry();
        Assert.assertEquals(m.toString(), "1=one");

        m = map.pollLastEntry();
        Assert.assertEquals(m, null);
    }

    public static void descendingMapTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        NavigableMap<Integer, String> m = map.descendingMap();
        Assert.assertEquals(m.toString(), "{2=two, 1=one}");
    }

    public static void navigableKeySetTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        NavigableSet<Integer> m = map.navigableKeySet();
        Assert.assertEquals(m.toString(), "[1, 2]");
    }

    public static void descendingKeySetTest() {
        NavigableMap<Integer, String> map = new TreeMap<>();
        map.put(1, "one");
        map.put(2, "two");

        NavigableSet<Integer> m = map.descendingKeySet();
        Assert.assertEquals(m.toString(), "[2, 1]");
    }

    public static void subMapTest() {
        NavigableMap<Integer, String> m = (NavigableMap<Integer, String>) map.subMap(2, 4);
        Assert.assertEquals(m.toString(), "{2=two, 3=three}");
    }

    public static void headMapTest() {
        NavigableMap<Integer, String> m = (NavigableMap<Integer, String>) map.headMap(4);
        Assert.assertEquals(m.toString(), "{1=one, 2=two, 3=three}");
    }

    public static void tailMapTest() {
        NavigableMap<Integer, String> m = (NavigableMap<Integer, String>) map.tailMap(9);
        Assert.assertEquals(m.toString(), "{9=nine, 10=ten}");
    }

    public static void subSortedMapTest() {
        SortedMap<Integer, String> m = map.subMap(2, 4);
        Assert.assertEquals(m.toString(), "{2=two, 3=three}");
    }

    public static void headSortedMapTest() {
        SortedMap<Integer, String> m = map.headMap(4);
        Assert.assertEquals(m.toString(), "{1=one, 2=two, 3=three}");
    }

    public static void tailSortedMapTest() {
        SortedMap<Integer, String> m = map.tailMap(9);
        Assert.assertEquals(m.toString(), "{9=nine, 10=ten}");
    }

    public static void main(String[] args) {
        mapInit();

//        lowerEntryTest();
//        lowerKeyTest();
//        floorEntryTest();
//        floorKeyTest();
//        ceilingEntryTest();
//        ceilingKeyTest();
//        higherEntryTest();
//        higherKeyTest();
//        firstEntryTest();
//        lastEntryTest();
//        pollFirstEntryTest();
//        pollLastEntryTest();
//        descendingMapTest();
//        navigableKeySetTest();
//        descendingKeySetTest();
//        subMapTest();
//        headMapTest();
//        tailMapTest();
//        subSortedMapTest();
//        headSortedMapTest();
//        tailSortedMapTest();
    }
}
