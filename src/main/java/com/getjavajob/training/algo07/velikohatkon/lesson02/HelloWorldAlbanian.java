package main.java.com.getjavajob.training.algo07.velikohatkon.lesson02;

import java.io.UnsupportedEncodingException;

/**
 * @author Vital Severyn
 */
public class HelloWorldAlbanian {
    public static void main(String[] args) throws UnsupportedEncodingException {
        String original = "P�rsh�ndetje bot�!";
        byte[] b = original.getBytes();
        String encoded = new String(b, "windows-1250");
        System.out.println(encoded);
    }
}
