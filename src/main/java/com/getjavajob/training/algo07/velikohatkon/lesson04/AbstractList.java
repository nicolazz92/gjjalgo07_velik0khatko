package main.java.com.getjavajob.training.algo07.velikohatkon.lesson04;

import com.sun.istack.internal.Nullable;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import static java.lang.System.arraycopy;

/**
 * Created by ������� on 27.10.2015.
 * on 10:56
 */
public abstract class AbstractList<E> implements List<E> {
    E[] mass;
    private int size;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        int i = searchObject((E) o);
        return (i == -1);
    }

    @Override
    public Iterator<E> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return mass;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(E e) {
        growIfFull();
        mass[size] = e;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        int i = searchObject((E) o);
        return i != -1 && rmObject(i);
    }

    boolean rmObject(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0 || i > size - 1) throw new ArrayIndexOutOfBoundsException("rmObject(int i): 'i' is wrong");
        arraycopy(mass, i + 1, mass, i, size - 1 - i);
        mass[size - 1] = null;
        size--;
        return true;
    }

    @Override
    abstract public boolean containsAll(Collection<?> c);

    @Override
    abstract public boolean addAll(Collection<? extends E> c);

    @Override
    abstract public boolean addAll(int index, Collection<? extends E> c);

    @Override
    abstract public boolean removeAll(Collection<?> c);

    @Override
    abstract public boolean retainAll(Collection<?> c);

    @Override
    public void clear() {

    }

    @Override
    public E get(int index) throws ArrayIndexOutOfBoundsException {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("get(int i): 'i' is too big");
        }
        return mass[index];
    }

    @Override
    public E set(int index, E element) throws ArrayIndexOutOfBoundsException {
        if (index >= mass.length) {
            throw new ArrayIndexOutOfBoundsException("set(int i, Object e): 'i' is too big");
        }
        E r = mass[index];
        mass[index] = element;
        return r;
    }

    @Override
    public void add(int index, E element) throws ArrayIndexOutOfBoundsException {
        if (index > size) {
            throw new ArrayIndexOutOfBoundsException("add(int i, Object e): 'i' is too big");
        }
        growIfFull();

        arraycopy(mass, index, mass, index + 1, size - index);

        mass[index] = element;
        size++;
    }

    @Override
    public E remove(int index) throws ArrayIndexOutOfBoundsException {
        if (index >= size) {
            throw new ArrayIndexOutOfBoundsException("remove(int i): 'i' is too big");
        }
        E r = mass[index];
        rmObject(index);
        return r;
    }

    boolean growIfFull() {
        if (size < mass.length - 1) {
            return false;
        }

        E[] bigger = (E[]) new Object[(int) (mass.length * 1.5)];
        arraycopy(mass, 0, bigger, 0, mass.length);
        mass = bigger;
        return true;
    }

    int searchObject(E elem) {
        for (int i = 0; i < size; i++) {
            if (elem == null && mass[i] == null) {
                return i;
            }
            if (mass[i].equals(elem)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int indexOf(Object o) throws ArrayIndexOutOfBoundsException {
        int i = searchObject((E) o);
        if (i == -1) {
            throw new ArrayIndexOutOfBoundsException("indexOf(Object e): There is no this element");
        }
        return i;
    }

    @Override
    abstract public int lastIndexOf(Object o);

    @Override
    abstract public ListIterator<E> listIterator();

    @Override
    abstract public ListIterator<E> listIterator(int index);

    @Override
    abstract public List<E> subList(int fromIndex, int toIndex);


}
