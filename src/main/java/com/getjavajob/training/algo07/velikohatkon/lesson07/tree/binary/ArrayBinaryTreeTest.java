package main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.binary;

/**
 * Created by Николай on 26.12.2015.
 * on 13:58
 */
public class ArrayBinaryTreeTest {

    public static void addTest() {
        ArrayBinaryTree<Integer> d = new ArrayBinaryTree<>();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);
        d.add(175);

        System.out.println();
    }

    public static void travelTest() {
        ArrayBinaryTree<Integer> d = new ArrayBinaryTree<>();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);
        d.add(175);

        System.out.println(d.root().getElement());
        System.out.println(d.left(d.root()).getElement());
        System.out.println(d.left(d.left(d.root())).getElement());

        System.out.println(d.root().getElement());
        System.out.println(d.right(d.root()).getElement());
        System.out.println(d.right(d.right(d.root())).getElement());

        System.out.println();
    }

    public static void iterTest() {
        ArrayBinaryTree a = new ArrayBinaryTree();
        ArrayBinaryTree.ArrBinTreeIterator<Integer> d = (ArrayBinaryTree.ArrBinTreeIterator<Integer>) a.iterator();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(25);
        d.add(175);

        if (d.hasRoot()) {
            d.root();
        }
        while (d.hasRight()) {
            d.right();
        }
        System.out.println(d.getValue());

        while (d.hasParent()) {
            d.parent();
        }
        System.out.println(d.getValue());

        while (d.hasLeft()) {
            d.left();
        }
        System.out.println(d.getValue());

        d.remove();

        System.out.println(d.getValue());

        System.out.println();
    }

    public static void main(String[] args) {
        addTest();
        travelTest();
        iterTest();
    }
}
