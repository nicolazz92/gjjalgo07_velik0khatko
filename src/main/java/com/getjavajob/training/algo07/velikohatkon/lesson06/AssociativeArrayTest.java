package main.java.com.getjavajob.training.algo07.velikohatkon.lesson06;

import main.java.com.getjavajob.training.algo07.util.Assert;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ������� on 25.11.2015.
 * on 10:58
 */
public class AssociativeArrayTest {
    public static void addTest() {
        AssociativeArray<Integer, String> d = new AssociativeArray<>();
        d.add(null, "zero");
        d.add(12, "twelve");
        d.add(11, "eleven");
        d.add(21, null);
        d.add(1, "one");
        d.add(31, "thirty one");
        Assert.assertEquals(d.add(11, "elevenV2"), "eleven");
        d.printMass();
    }

    public static void removeTest() {
        AssociativeArray<Integer, String> d = new AssociativeArray<>();
        d.add(null, "zero");
        d.add(12, "twelve");
        d.add(11, "eleven");
        d.add(21, null);
        d.add(1, "one");
        d.add(31, "thirty one");
        Assert.assertEquals(d.remove(31), "thirty one");
        System.out.println(d.remove(21));
        Assert.assertEquals(d.remove(11), "eleven");
        Assert.assertEquals(d.remove(1), "one");
        d.printMass();
    }

    public static void getTest() {
        AssociativeArray<Integer, String> d = new AssociativeArray<>();
        d.add(null, "zero");
        d.add(12, "twelve");
        d.add(11, "eleven");
        d.add(21, null);
        d.add(1, "one");
        d.add(31, "thirty one");
        d.printMass();
        System.out.println();
        System.out.println(d.get(1));
        System.out.println(d.get(11));
        System.out.println(d.get(21));
        System.out.println(d.get(null));
    }

    public static void growTest(){
        AssociativeArray<Integer, String> d = new AssociativeArray<>();
        d.add(0, "zero");
        d.add(10, "ten");
        d.add(1, "one");
        d.add(2, "two");
        d.add(3, "three");
        d.add(4, "four");
        d.printMass();
        System.out.println();
        d.add(5, "five");
        d.printMass();
        System.out.println();
        d.add(6, "six");
        d.add(7, "seven");
        d.add(8, "eight");
        d.add(9, "nine");
        d.add(30, "thirty");
        d.add(31, "thirty one");
        d.printMass();
        System.out.println("\nsize = " + d.getSize());
    }

    public static void strangeKeysTest(){
        AssociativeArray<String, Integer> d = new AssociativeArray<>();
        d.add("str", 12);
        d.add("polygenelubricants", 1);
        d.add("GydZG_", 2);
        d.add("DESIGNING WORKHOUSES", 3);
        d.add("QWERTYUIOPASDFGHJKLZXCVBNM__QWERTYUIOPASDFGHJKLZXCVBNM", 4);
        d.printMass();
        System.out.println("\n" + d.get("str"));
        Assert.assertEquals(d.get("polygenelubricants"), new Integer(1));
        Assert.assertEquals(d.get("GydZG_"), new Integer(2));
        Assert.assertEquals(d.get("DESIGNING WORKHOUSES"), new Integer(3));
        Assert.assertEquals(d.get("QWERTYUIOPASDFGHJKLZXCVBNM__QWERTYUIOPASDFGHJKLZXCVBNM"), new Integer(4));

        Map < String, Integer > b = new HashMap<>();
        b.put("polygenelubricants", 1);
        b.put("GydZG_", 2);
        b.put("DESIGNING WORKHOUSES", 3);
        Assert.assertEquals(b.get("polygenelubricants"), new Integer(1));
    }

    public static void main(String[] args) {
        addTest();
        removeTest();
        getTest();
        growTest();
        strangeKeysTest();
    }
}