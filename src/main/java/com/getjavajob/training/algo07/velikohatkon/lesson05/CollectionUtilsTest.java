package main.java.com.getjavajob.training.algo07.velikohatkon.lesson05;

import main.java.com.getjavajob.training.algo07.util.Assert;
import main.java.com.getjavajob.training.algo07.velikohatkon.lesson04.SinglyLinkedList;

/**
 * Created by ������� on 05.11.2015.
 * on 14:24
 */
public class CollectionUtilsTest {

    public static void filterModificatingTest(){
        SinglyLinkedList<Integer> d = new SinglyLinkedList<>();
        d.add(10); d.add(8); d.add(6); d.add(4); d.add(2);
        CollectionUtils.filterModificating(d);
        Assert.assertEquals(d.print(), "10 8 6 ");
    }

    public static void filterReturningTest(){
        SinglyLinkedList<Integer> d = new SinglyLinkedList<>();
        d.add(10); d.add(8); d.add(6); d.add(4); d.add(2);
        SinglyLinkedList<Integer> res = CollectionUtils.filterReturning(d);
        Assert.assertEquals(res.print(), "10 8 6 ");
    }

    public static void transformModificatingTest(){
        SinglyLinkedList<Integer> d = new SinglyLinkedList<>();
        d.add(10); d.add(8); d.add(6); d.add(4); d.add(2);
        CollectionUtils.trasformModificating(d);
        Assert.assertEquals(d.print(), "10 8 6 4 2 ");
    }

    public static void transformReturningTest(){
        SinglyLinkedList<Integer> d = new SinglyLinkedList<>();
        d.add(10); d.add(8); d.add(6); d.add(4); d.add(2);
        CollectionUtils.trasformModificating(d);
        Assert.assertEquals(CollectionUtils.trasformReturning(d).print(), "10.0 8.0 6.0 4.0 2.0 ");
    }

    public static void doForAllTest(){
        SinglyLinkedList<Integer> d = new SinglyLinkedList<>();
        d.add(10); d.add(8); d.add(6); d.add(4); d.add(2);
        CollectionUtils.forAllDo(d);
        Assert.assertEquals(d.print(), "10 8 6 8 4 ");
    }

    public static void unmodifiableCollectionTest(){
        SinglyLinkedList<Integer> d = new SinglyLinkedList<>();
        d.add(10); d.add(8); d.add(6); d.add(4); d.add(2);
        unmodCollection res = CollectionUtils.unmodifiableCollection(d);
        try {
            res.add(12);
        } catch (UnsupportedOperationException e){
            Assert.assertEquals(e.getMessage(), "collection is unmodifiable");
        }
        Assert.assertEquals(res.print(), "10 8 6 4 2 ");
    }

    public static void main(String[] args) {
        filterModificatingTest();
        filterReturningTest();
        transformModificatingTest();
        transformReturningTest();
        doForAllTest();
        unmodifiableCollectionTest();
    }
}