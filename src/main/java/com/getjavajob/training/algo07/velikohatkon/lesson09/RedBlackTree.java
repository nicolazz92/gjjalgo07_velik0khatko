package main.java.com.getjavajob.training.algo07.velikohatkon.lesson09;

import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;
import main.java.com.getjavajob.training.algo07.velikohatkon.lesson08.binary.search.balanced.TreeDrawer;

/**
 * @author Vital Severyn
 * @since 05.08.15
 */
public class RedBlackTree<E> extends TreeDrawer<E> {

    private boolean isRed(Node<E> n) {
        return n != null && validateColor(n).isRed();
    }

    private boolean isBlack(Node<E> n) {
        return n == null || !isRed(n);
    }

    private void makeBlack(Node<E> n) {
        validateColor(n).setBlack();
    }

    private void makeRed(Node<E> n) {
        validateColor(n).setRed();
    }

    private void afterElementAdded(Node<E> node) {
        if (validateColor(node) != null && !validate(node).equals(validate(getRoot()))) {
            //z is left subtree
            if (node == validateColor(validateColor(node).getParent()).getLeft()) {
                addCaseOneLeft(node);
                addCaseTwoLeft(node);
                addCaseThreeLeft(node);
            } else {
                addCaseOneRight(node);
                addCaseTwoRight(node);
                addCaseThreeRight(node);
            }
            afterElementAdded(validateColor(node).getParent());
        }
    }

    private void addCaseOneLeft(Node<E> node) {
        Node<E> z;
        Node<E> p;
        Node<E> g;
        Node<E> u;
        try {
            z = node;
            p = validateColor(z).getParent();
            g = validateColor(p).getParent();
            u = validateColor(g).getRight();
            if (!(isRed(z) && isRed(p) && isBlack(g) && isRed(u)) ||
                    z != validateColor(validateColor(g).getLeft()).getLeft()) {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            return;
        }
        makeBlack(p);
        makeBlack(u);
        makeRed(g);

        makeBlack(getRoot());
    }

    private void addCaseTwoLeft(Node<E> node) {
        Node<E> z;
        Node<E> p;
        Node<E> g;
        Node<E> u;
        try {
            z = node;
            p = validateColor(z).getParent();
            g = validateColor(p).getParent();
            u = validateColor(g).getLeft();
            if (!(isRed(z) && isRed(p) && isBlack(g) && isBlack(u))) {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            return;
        }
        rotate(z);

        makeBlack(getRoot());

        addCaseThreeRight(validateColor(z).getRight());
    }

    private void addCaseThreeLeft(Node<E> node) {
        Node<E> z;
        Node<E> p;
        Node<E> g;
        Node<E> u;
        try {
            z = node;
            p = validateColor(z).getParent();
            g = validateColor(p).getParent();
            u = validateColor(g).getRight();
            if (!(isRed(z) && isRed(p) && isBlack(g) && isBlack(u))) {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            return;
        }
        makeBlack(p);
        makeRed(g);

        rotate(p);

        makeBlack(getRoot());

        afterElementAdded(validateColor(z).getParent());
    }

    private void addCaseOneRight(Node<E> node) {
        Node<E> z;
        Node<E> p;
        Node<E> g;
        Node<E> u;
        try {
            z = node;
            p = validateColor(z).getParent();
            g = validateColor(p).getParent();
            u = validateColor(g).getLeft();
            if (!(isRed(z) && isRed(p) && isBlack(g) && isRed(u)) ||
                    z != validateColor(validateColor(g).getRight()).getRight()) {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            return;
        }
        makeBlack(p);
        makeBlack(u);
        makeRed(g);

        makeBlack(getRoot());
    }

    private void addCaseTwoRight(Node<E> node) {
        Node<E> z;
        Node<E> p;
        Node<E> g;
        Node<E> u;
        try {
            z = node;
            p = validateColor(z).getParent();
            g = validateColor(p).getParent();
            u = validateColor(g).getRight();
            if (!(isRed(z) && isRed(p) && isBlack(g) && isBlack(u))) {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            return;
        }
        rotate(z);

        makeBlack(getRoot());

        addCaseThreeLeft(validateColor(z).getLeft());
    }

    private void addCaseThreeRight(Node<E> node) {
        Node<E> z;
        Node<E> p;
        Node<E> g;
        Node<E> u;
        try {
            z = node;
            p = validateColor(z).getParent();
            g = validateColor(p).getParent();
            u = validateColor(g).getLeft();
            if (!(isRed(z) && isRed(p) && isBlack(g) && isBlack(u))) {
                throw new NullPointerException();
            }
        } catch (NullPointerException e) {
            return;
        }
        makeBlack(p);
        makeRed(g);

        rotate(p);

        makeBlack(getRoot());

//        afterElementAdded(validateColor(z).getParent());
    }

    private void afterElementRemoved(Node<E> n) {
        treePrint();
        System.out.println("--------------------------------");

        if (n == null) {
            return;
        }

        Node<E> x = validateColor(n);
        while (x != getRoot() && validateColor(x).isBlack()) {
            if (x == validateColor(validateColor(x).getParent()).getLeft()) {//LeftCase
                Node<E> w = validateColor(validateColor(x).getParent()).getRight();
                if (isRed(validateColor(w))) {
                    ////////////// Case 1
                    makeBlack(w);
                    makeRed(validateColor(x));
                    rotate(validateColor(validateColor(x).getParent()).getRight());//LEFT-ROTATE
                    w = validateColor(validateColor(x).getParent()).getRight();
                }
                //if w.left.color == BLACK и w.right.color == BLACK
                if (isBlack(validateColor(validateColor(w).getLeft())) && isBlack(validateColor(validateColor(w).getRight()))) {
                    /////////////// Case 2
                    makeRed(w);
                    x = validateColor(x).getParent();
                } else if (isBlack(validateColor(validateColor(w).getRight()))) {
                    ///////////// Case 3
                    makeBlack(validateColor(w).getLeft());
                    makeRed(w);
                    rotate(validateColor(w).getLeft());//RIGHT-ROTATE
                    w = validateColor(validateColor(x).getParent()).getRight();
                    ////////////// Case 4
                    //w.color = x.p.color
                    if (isRed(validateColor(validateColor(x).getParent()))) {
                        makeRed(w);
                    } else {
                        makeBlack(w);
                    }
                    //
                    makeBlack(validateColor(validateColor(x)).getParent());
                    makeBlack(validateColor(w).getRight());
                    rotate(validateColor(x).getRight());//LEFT-ROTATE
                    x = getRoot();
                }
            } else {// Right Case
                Node<E> w = validateColor(validateColor(x).getParent()).getLeft();
                if (isRed(validateColor(w))) {
                    ////////////// Case 1
                    makeBlack(w);
                    makeRed(validateColor(x));
                    rotate(validateColor(validateColor(x).getParent()).getLeft());//LEFT-ROTATE
                    w = validateColor(validateColor(x).getParent()).getLeft();
                }
                //if w.left.color == BLACK и w.right.color == BLACK
                if (isBlack(validateColor(validateColor(w).getRight())) && isBlack(validateColor(validateColor(w).getLeft()))) {
                    /////////////// Case 2
                    makeRed(w);
                    x = validateColor(x).getParent();
                } else if (isBlack(validateColor(validateColor(w).getLeft()))) {
                    ///////////// Case 3
                    makeBlack(validateColor(w).getRight());
                    makeRed(w);
                    rotate(validateColor(w).getRight());//RIGHT-ROTATE
                    w = validateColor(validateColor(x).getParent()).getLeft();
                    ////////////// Case 4
                    //w.color = x.p.color
                    if (isRed(validateColor(validateColor(x).getParent()))) {
                        makeRed(w);
                    } else {
                        makeBlack(w);
                    }
                    //
                    makeBlack(validateColor(validateColor(x)).getParent());
                    makeBlack(validateColor(w).getLeft());
                    rotate(validateColor(x).getLeft());//LEFT-ROTATE
                    x = getRoot();
                }
            }
            makeBlack(x);
        }

        treePrint();
        System.out.println();
    }

    @Override
    public E removeElem(E a, Node<E> node) {
        Node<E> r = treeSearch(node, a);
        boolean rightChild = r == validateColor(validateColor(r).getParent()).getRight();
        r = validateColor(r).getParent();
        Node<E> res;
        super.removeElem(a, node);
        if (rightChild) {
            res = validateColor(r).getRight();
        } else {
            res = validateColor(r).getLeft();
        }
        afterElementRemoved(res);
        return res == null ? null : res.getElement();
    }

    @Override
    public Node<E> addLeft(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("parent is null");
        }
        NodeImplColor<E> d = validateColor(n);
        if (d.getLeft() == null) {
            d.setLeft(new NodeImplColor<>(e));
            validateColor(d.getLeft()).setParent(d);
            sizePlus();
            Node<E> res = d.getLeft();

            treePrint();
            System.out.println("--------------------------------");
            afterElementAdded(res);
            treePrint();

            return res;
        } else {
            throw new IllegalArgumentException("node already has a left child");
        }
    }

    @Override
    public Node<E> addRight(Node<E> n, E e) throws IllegalArgumentException {
        if (n == null) {
            throw new IllegalArgumentException("parent is null");
        }
        NodeImplColor<E> d = validateColor(n);
        if (d.getRight() == null) {
            d.setRight(new NodeImplColor<>(e));
            validateColor(d.getRight()).setParent(d);
            sizePlus();
            Node<E> res = d.getRight();

            treePrint();
            System.out.println("--------------------------------");
            afterElementAdded(res);
            treePrint();

            return res;
        } else {
            throw new IllegalArgumentException("node already has a right child");
        }
    }

    public Node<E> addRoot(E e) throws IllegalStateException {
        try {
            NodeImplColor<E> node = new NodeImplColor<>(e);
            makeBlack(node);
            super.setRoot(node);
            return super.getRoot();
        } catch (IllegalArgumentException a) {
            System.out.println("wrong arg type");
            return null;
        }
    }

    public NodeImplColor<E> validateColor(Node<E> n) throws IllegalArgumentException {
        if (n == null) {
            return null;
        }
        try {
            return (NodeImplColor<E>) super.validate(n);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    public static class NodeImplColor<E> extends NodeImpl<E> {
        boolean red;

        public NodeImplColor(E data) {
            super(data);
            this.red = true;
        }

        public boolean isRed() {
            return red;
        }

        public boolean isBlack() {
            return !isRed();
        }

        public void setRed() {
            this.red = true;
        }

        public void setBlack() {
            this.red = false;
        }
    }

}