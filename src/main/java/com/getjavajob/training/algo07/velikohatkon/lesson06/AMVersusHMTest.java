package main.java.com.getjavajob.training.algo07.velikohatkon.lesson06;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;

/**
 * Created by ������� on 27.11.2015.
 * on 12:34
 */
public class AMVersusHMTest {
    static final double nanoToMilli = 1_000_000;
    static final int addRmIndex = 5_000_000;
    static final String fileName = "src/main/java/com/getjavajob/training/algo07/velikohatkon/lesson06/AMVersusHMTest.txt";

    public static void deleteFile(String filename) throws FileNotFoundException {
        File file = new File(fileName);
        file.exists();
        new File(filename).delete();
    }

    public static void write(String fileName, String text) {
        File file = new File(fileName);

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (FileWriter writer = new FileWriter(fileName, true)) {
            // ������ ���� ������
            writer.append(text);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void addGetRm() {
        Map<Integer, String> hashMap = new HashMap<>();
        AssociativeArray<Integer, String> associativeArray = new AssociativeArray<>();

        write(fileName, "Add\n");
        write(fileName, "================================\n");
        System.out.println("Add");
        System.out.println("================================");

        long AMAddTimeStart = System.nanoTime();

        for(int i = 0; i < addRmIndex; i++){
            associativeArray.add(i, String.valueOf(i));
        }

        long AMAddTimeFinish = System.nanoTime();
        long AMAddTime = AMAddTimeFinish - AMAddTimeStart;
        write(fileName, format("AssociativeArray.add(key, value): %.2f ms\n", AMAddTime / nanoToMilli));
        System.out.printf("AssociativeArray.add(key, value): %.2f ms\n", AMAddTime / nanoToMilli);

        ////////////////////

        long HMAddTimeStart = System.nanoTime();

        for(int i = 0; i < addRmIndex; i++){
            hashMap.put(i, String.valueOf(i));
        }

        long HMAddTimeFinish = System.nanoTime();
        long HMAddTime = HMAddTimeFinish - HMAddTimeStart;
        write(fileName, format("HashMap.put(key, val): %.2f ms\n", HMAddTime / nanoToMilli));
        System.out.printf("HashMap.put(key, val): %.2f ms\n", HMAddTime / nanoToMilli);

        write(fileName, format("AM/HM = %.2f / 1\n", AMAddTime / (double) HMAddTime));
        System.out.printf("AM/HM = %.2f / 1\n", AMAddTime / (double) HMAddTime);

        write(fileName, "Get\n");
        write(fileName, "================================\n");
        System.out.println("Get");
        System.out.println("================================");

        long AMGetTimeStart = System.nanoTime();

        for(int i = 0; i < addRmIndex; i++){
            associativeArray.get(i);
        }

        long AMGetTimeFinish = System.nanoTime();
        long AMGetTime = AMGetTimeFinish - AMGetTimeStart;
        write(fileName, format("AssociativeArray.get(key): %.2f ms\n", AMGetTime / nanoToMilli));
        System.out.printf("AssociativeArray.get(key): %.2f ms\n", AMGetTime / nanoToMilli);

        ////////////////////

        long HMGetTimeStart = System.nanoTime();

        for(int i = 0; i < addRmIndex; i++){
            hashMap.get(i);
        }

        long HMGetTimeFinish = System.nanoTime();
        long HMGetTime = HMGetTimeFinish - HMGetTimeStart;
        write(fileName, format("HashMap.get(key): %.2f ms\n", HMGetTime / nanoToMilli));
        System.out.printf("HashMap.get(key): %.2f ms\n", HMGetTime / nanoToMilli);

        write(fileName, format("AM/HM = %.2f / 1\n", AMGetTime / (double) HMGetTime));
        System.out.printf("AM/HM = %.2f / 1\n", AMGetTime / (double) HMGetTime);

        write(fileName, "Remove\n");
        write(fileName, "================================\n");
        System.out.println("Remove");
        System.out.println("================================");

        long AMRemoveTimeStart = System.nanoTime();

        for(int i = 0; i < addRmIndex; i++){
            associativeArray.remove(i);
        }

        long AMRemoveTimeFinish = System.nanoTime();
        long AMRemoveTime = AMRemoveTimeFinish - AMRemoveTimeStart;
        write(fileName, format("AssociativeArray.remove(key): %.2f ms\n", AMRemoveTime / nanoToMilli));
        System.out.printf("AssociativeArray.remove(key): %.2f ms\n", AMRemoveTime / nanoToMilli);

        long HMRemoveTimeStart = System.nanoTime();

        for(int i = 0; i < addRmIndex; i++){
            hashMap.remove(i);
        }

        long HMRemoveTimeFinish = System.nanoTime();
        long HMRemoveTime = HMRemoveTimeFinish - HMRemoveTimeStart;
        write(fileName, format("HashMap.remove(key): %.2f ms\n", HMRemoveTime / nanoToMilli));
        System.out.printf("HashMap.remove(key): %.2f ms\n", HMRemoveTime / nanoToMilli);

        write(fileName, format("AM/HM = %.2f / 1\n", AMRemoveTime / (double) HMRemoveTime));
        System.out.printf("AM/HM = %.2f / 1\n", AMRemoveTime / (double) HMRemoveTime);

        write(fileName, "================================\n");
        System.out.println("================================");

    }

    public static void main(String[] args) throws FileNotFoundException {
        deleteFile(fileName);
        addGetRm();
    }
}
