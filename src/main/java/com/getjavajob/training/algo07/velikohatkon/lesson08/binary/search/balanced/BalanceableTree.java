package main.java.com.getjavajob.training.algo07.velikohatkon.lesson08.binary.search.balanced;

import main.java.com.getjavajob.training.algo07.velikohatkon.lesson07.tree.Node;
import main.java.com.getjavajob.training.algo07.velikohatkon.lesson08.binary.search.BinarySearchTree;

public class BalanceableTree<E> extends BinarySearchTree<E> {

    /**
     * makeLeftChild - left rotation if true, right rotation if false.
     * Relinks a parent with child node.
     */
    public void relink(NodeImpl<E> parent, NodeImpl<E> child, boolean makeLeftChild) {
        if (makeLeftChild) {
            leftRotation(parent, child);
        }
        if (!makeLeftChild) {
            rightRotation(parent, child);
        }
    }

    /**
     * Call by relink()
     * https://en.wikipedia.org/wiki/Tree_rotation#/media/File:Tree_Rotations.gif
     */
    private void rightRotation(NodeImpl<E> parent, NodeImpl<E> child) {
        //is parent == root
        boolean root = false;
        //what child is parent
        boolean rightChild = false;
        NodeImpl<E> parentOfParent = null;

        if (parent.getParent() == null) {
            root = true;
        } else {
            parentOfParent = validate(parent.getParent());
            rightChild = parentOfParent.getRight() == parent;
        }

        parent.setLeft(validate(child.getRight()));
        if (child.getRight() != null) {
            validate(child.getRight()).setParent(validate(parent));
        }

        child.setRight(parent);
        parent.setParent(child);

        if (root) {
            setRoot(child);
        } else {
            child.setParent(parentOfParent);
            if (rightChild) {
                parentOfParent.setRight(child);
            } else {
                parentOfParent.setLeft(child);
            }
        }
    }

    /**
     * Call by relink()
     * https://en.wikipedia.org/wiki/Tree_rotation#/media/File:Tree_Rotations.gif
     */
    private void leftRotation(NodeImpl<E> parent, NodeImpl<E> child) {
        //is parent == root
        boolean root = false;
        //what child is parent
        boolean rightChild = false;
        NodeImpl<E> parentOfParent = null;

        if (parent.getParent() == null) {
            root = true;
        } else {
            parentOfParent = validate(parent.getParent());
            rightChild = parentOfParent.getRight() == parent;
        }

        parent.setRight(validate(child.getLeft()));
        if (child.getLeft() != null) {
            validate(child.getLeft()).setParent(validate(parent));
        }

        child.setLeft(parent);
        parent.setParent(child);

        if (root) {
            setRoot(child);
        } else {
            child.setParent(parentOfParent);
            if (rightChild) {
                parentOfParent.setRight(child);
            } else {
                parentOfParent.setLeft(child);
            }
        }
    }

    /**
     * Rotates n with parent.
     * call relink()
     */
    public void rotate(Node<E> node) throws IllegalArgumentException {
        NodeImpl<E> n = validate(node);
        if (n == null) {
            return;
        }
        if (n.getParent() == null) {
            throw new IllegalArgumentException("Argument node has not parent");
        }

        if (validate(n.getParent()).getRight() == node) {
            relink(validate(n.getParent()), n, true);
        } else if (validate(n.getParent()).getLeft() == node) {
            relink(validate(n.getParent()), n, false);
        }
    }

    /**
     * Performs a left-right/right-left rotations.
     */
    public Node<E> rotateTwice(Node<E> node) throws IllegalArgumentException {
        NodeImpl<E> n = validate(node);

        NodeImpl<E> grandFather;
        try {
            grandFather = validate(validate(n.getParent()).getParent());
            grandFather.getLeft();
        } catch (NullPointerException e) {
            throw new IllegalArgumentException("Node has not grandfather");
        }

        //to except left-left and right-right rotations
        boolean letMeRotate = false;
        try {
            validate(validate(grandFather.getRight()).getLeft()).getElement();
            letMeRotate = true;
        } catch (NullPointerException ignored) {
        }
        try {
            validate(validate(grandFather.getLeft()).getRight()).getElement();
            letMeRotate = true;
        } catch (NullPointerException ignored) {
        }

        if (letMeRotate) {
            rotate(node);
            rotate(node);
            return node;
        } else {
            throw new IllegalArgumentException("Opposite rotations doesn't possible");
        }
    }

}
