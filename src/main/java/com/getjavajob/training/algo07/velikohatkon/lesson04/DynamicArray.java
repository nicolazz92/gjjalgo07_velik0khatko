package main.java.com.getjavajob.training.algo07.velikohatkon.lesson04;

import static java.lang.System.arraycopy;

/**
 * Created by nicolas on 29.08.15.
 * at 18:03
 */
public class DynamicArray<E> {
    public E[] mass;
    private int size;

    public DynamicArray() {
        int BEGIN_SIZE = 10;
        mass = (E[]) new Object[BEGIN_SIZE];
        size = 0;
    }

    public DynamicArray(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0) {
            throw new ArrayIndexOutOfBoundsException("DynamicArray(int i): i < 0");
        }
        mass = (E[]) new Object[i];
        size = 0;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public E[] getMass() {
        return mass;
    }

    public void setMass(E[] mass) {
        this.mass = mass;
    }

    public static class ListIteratorImpl<E> implements java.util.ListIterator<E> {

        private int iterIndex = 0;
        private DynamicArray<E> d;

        public ListIteratorImpl(int i) {
            d = new DynamicArray<>(i);
        }

        @Override
        public boolean hasNext() {
            return (iterIndex < d.size() - 1);
        }

        @Override
        public E next() {
            iterIndex++;
            return d.mass[iterIndex];
        }

        @Override
        public boolean hasPrevious() {
            return (iterIndex > 0);
        }

        @Override
        public E previous() {
            iterIndex--;
            return d.mass[iterIndex];
        }

        @Override
        public int nextIndex() {
            return iterIndex++;
        }

        @Override
        public int previousIndex() {
            return iterIndex--;
        }

        @Override
        public void remove() {
            d.rmObject(iterIndex);
        }

        @Override
        public void set(E e) {
            d.set(iterIndex, e);
        }

        @Override
        public void add(E e) {
            d.add(iterIndex, e);
            iterIndex++;
        }
    }

    boolean add(E e) {
        growIfFull();
        mass[size] = e;
        size++;
        return true;
    }

    void add(int i, E e) throws ArrayIndexOutOfBoundsException {
        if (i > size) {
            throw new ArrayIndexOutOfBoundsException("add(int i, Object e): 'i' is too big");
        }
        growIfFull();

        arraycopy(mass, i, mass, i + 1, size - i);

        mass[i] = e;
        size++;
    }

    boolean rmObject(int i) throws ArrayIndexOutOfBoundsException {
        if (i < 0 || i > size - 1) throw new ArrayIndexOutOfBoundsException("rmObject(int i): 'i' is wrong");
        arraycopy(mass, i + 1, mass, i, size - 1 - i);
        mass[size - 1] = null;
        size--;
        return true;
    }

    boolean remove(E e) {
        int i = searchObject(e);
        if (i == -1) {
            return false;
        }
        rmObject(i);
        return true;
    }

    E remove(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("remove(int i): 'i' is too big");
        }
        E r = mass[i];
        rmObject(i);
        return r;
    }

    public boolean growIfFull() {
        if (size() < getMass().length - 1) {
            return false;
        }

        E[] bigger = (E[]) new Object[(int) (getMass().length * 1.5)];
        arraycopy(getMass(), 0, bigger, 0, getMass().length);
        setMass(bigger);
        return true;
    }

    E set(int i, E e) throws ArrayIndexOutOfBoundsException {
        if (i >= mass.length) {
            throw new ArrayIndexOutOfBoundsException("set(int i, Object e): 'i' is too big");
        }
        E r = mass[i];
        mass[i] = e;
        return r;
    }

    E get(int i) throws ArrayIndexOutOfBoundsException {
        if (i >= size) {
            throw new ArrayIndexOutOfBoundsException("get(int i): 'i' is too big");
        }
        return mass[i];
    }

    int searchObject(E elem) {
        for (int i = 0; i < size; i++) {
            if (elem == null && mass[i] == null) {
                return i;
            }
            if (mass[i].equals(elem)) {
                return i;
            }
        }
        return -1;
    }

    public int size() {
        return size;
    }

    int indexOf(E e) throws ArrayIndexOutOfBoundsException {
        int i = searchObject(e);
        if (i == -1) {
            throw new ArrayIndexOutOfBoundsException("indexOf(Object e): There is no this element");
        }
        return i;
    }

    boolean contains(E e) {
        return searchObject(e) == -1;
    }

    Object[] toArray() {
        return mass;
    }
}
