package main.java.com.getjavajob.training.algo07.velikohatkon.lesson03;

import main.java.com.getjavajob.training.algo07.util.Assert;

/**
 * Created by nicolas on 08.09.15.
 * on 10:47
 */
public class DynamicArrayTest {
    public static void DynamicArrayTest1() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.hashCode();

            Assert.fail("DynamicArray() finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "DynamicArray() finished correctly");
        }
    }

    public static void DynamicArrayITest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.hashCode();

            Assert.fail("DynamicArray(int i) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "DynamicArray(int i): i < 0");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "DynamicArray(int i) finished correctly");
        }

        try {
            DynamicArray d = new DynamicArray(-1);
            d.hashCode();

            Assert.fail("DynamicArray(int i) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "DynamicArray(int i): i < 0");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "DynamicArray(int i) finished correctly");
        }
    }

    public static void addObjectTest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.add("first");
            printMass(d.toArray());
            d.add("second");
            printMass(d.toArray());
            d.add("third");
            printMass(d.toArray());
            d.add("fourth");
            printMass(d.toArray());
            d.add("five");
            printMass(d.toArray());
            d.add("six");
            printMass(d.toArray());
            d.add("seven");
            printMass(d.toArray());

            Assert.fail("add(Object e) finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "add(Object e) finished correctly");
        }
    }

    public static void addIObjectTest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.add(0, "second");
            printMass(d.toArray());
            d.add(1, "fourth");
            printMass(d.toArray());
            d.add(1, "third");
            printMass(d.toArray());
            d.add(0, "first");
            printMass(d.toArray());
            d.add(0, "zero");
            printMass(d.toArray());

            Assert.fail("add(int i, Object e) finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "add(int i, Object e) finished correctly");
        }

        try {
            DynamicArray d = new DynamicArray(3);
            d.add(0, "first");
            printMass(d.toArray());
            d.add(1, "third");
            printMass(d.toArray());
            d.add(1, "second");
            printMass(d.toArray());
            d.add(0, "zero");
            printMass(d.toArray());
            d.add(5, "third");

            Assert.fail("add(int i, Object e) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "add(int i, Object e): 'i' is too big");
        }
    }

    public static void setIObjectTest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.set(0, "first");
            d.set(1, "second");
            d.set(2, "third");

            Assert.fail("set(int i, Object e) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "set(int i, Object e): 'i' is too big");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "set(int i, Object e) finished correctly");
        }

        try {
            DynamicArray d = new DynamicArray(3);
            d.set(0, "first");
            d.set(1, "second");
            d.set(2, "third");
            d.set(3, "fourth");

            Assert.fail("set(int i, Object e) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "set(int i, Object e): 'i' is too big");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "set(int i, Object e) finished correctly");
        }
    }

    public static void getITest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.set(0, "first");
            d.set(1, "second");
            d.set(2, "third");
            d.get(0);
            d.get(1);
            d.get(2);

            Assert.fail("get(int i) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "get(int i): 'i' is too big");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "get(int i) finished correctly");
        }

        try {
            DynamicArray d = new DynamicArray(3);
            d.set(0, "first");
            d.set(1, "second");
            d.set(2, "third");
            d.get(0);
            d.get(1);
            d.get(2);
            d.get(3);

            Assert.fail("get(int i) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "get(int i): 'i' is too big");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "get(int i) finished correctly");
        }
    }

    public static void removeObjectTest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.add("first");
            d.add("second");
            d.add("third");
            d.add(null);
            d.add("fourth");
            printMass(d.toArray());

            d.remove("second");
            printMass(d.toArray());
            d.remove("first");
            printMass(d.toArray());
            d.remove("third");
            printMass(d.toArray());
            d.remove(null);
            printMass(d.toArray());

            Assert.fail("remove(Object e) finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "remove(Object e) finished correctly");
        }

    }

    public static void removeITest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.add("first");
            d.add("second");
            d.add("third");
            d.add("fourth");
            d.add("five");
            d.add("six");
            d.add("seven");
            printMass(d.toArray());
            d.remove(0);
            printMass(d.toArray());
            d.remove(3);
            printMass(d.toArray());

            Assert.fail("remove(int i) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "remove(int i): 'i' is too big");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "remove(int i) finished correctly");
        }

        try {
            DynamicArray d = new DynamicArray(3);
            d.set(0, "first");
            d.remove(3);

            Assert.fail("remove(int i) finished correctly");
        } catch (ArrayIndexOutOfBoundsException e1) {
            Assert.assertEquals(e1.getMessage(), "remove(int i): 'i' is too big");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "remove(int i) finished correctly");
        }
    }

    public static void sizeTest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.size();

            Assert.fail("size() finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "size() finished correctly");
        }
    }

    public static void indexOfObjectTest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.add("first");
            d.add("second");
            d.add("third");
            printMass(d.toArray());
            System.out.println(d.indexOf("first"));

            Assert.fail("indexOf(Object e) finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "indexOf(Object e) finished correctly");
        }
    }

    public static void containsObjectTest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.set(0, "first");
            d.set(1, "second");
            d.set(2, "third");
            d.contains("second");

            Assert.fail("contains(Object e) finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "contains(Object e) finished correctly");
        }

        try {
            DynamicArray d = new DynamicArray(3);
            d.set(0, "first");
            d.set(1, "second");
            d.set(2, "third");
            d.contains("fourth");

            Assert.fail("contains(Object e) finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "contains(Object e) finished correctly");
        }
    }

    public static void toArrayTest() {
        try {
            DynamicArray d = new DynamicArray(3);
            d.set(0, "first");
            d.set(1, "second");
            d.set(2, "third");
            d.toArray();

            Assert.fail("toArray() finished correctly");
        } catch (AssertionError e2) {
            Assert.assertEquals(e2.getMessage(), "toArray() finished correctly");
        }
    }

    public static void printMass(Object[] mas) {
        for (Object ma : mas) {
            System.out.print(ma + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        DynamicArrayTest1();
        DynamicArrayITest();
        addObjectTest();
        addIObjectTest();
        setIObjectTest();
        getITest();
        removeObjectTest();
        removeITest();
        sizeTest();
        indexOfObjectTest();
        containsObjectTest();
        toArrayTest();
    }
}
