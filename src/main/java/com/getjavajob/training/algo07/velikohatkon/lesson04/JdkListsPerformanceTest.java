package main.java.com.getjavajob.training.algo07.velikohatkon.lesson04;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

/**
 * Created by ������� on 18.10.2015
 * at 3:15
 */
public class JdkListsPerformanceTest {

    static final double nanoToMilli = 1_000_000;
    static final String obj = "obj";
    static final int addRmIndex = 100_000;
    static final String fileName = "src/main/java/com/getjavajob/training/algo07/velikohatkon/lesson04_v3/JdkListsPerformanceTest.txt";


    public static void deleteFile(String filename) throws FileNotFoundException {
        File file = new File(fileName);
        file.exists();
        new File(filename).delete();
    }

    public static void write(String fileName, String text) {
        File file = new File(fileName);

        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try (FileWriter writer = new FileWriter(fileName, true)) {
            // ������ ���� ������
            writer.append(text);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }

    }

    public static void addRmToBegin() {
        List<String> list = new LinkedList<>();
        List<String> AL = new ArrayList<>();

        write(fileName, "Add To Begin\n");
        write(fileName, "================================\n");
        System.out.println("Add To Begin");
        System.out.println("================================");

        long arrAddTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            AL.add(obj);
        }
        long arrAddTimeFinish = System.nanoTime();
        long arrAddTime = arrAddTimeFinish - arrAddTimeStart;
        write(fileName, format("ArrayList.add(e): %.2f ms\n", arrAddTime / nanoToMilli));
        System.out.printf("ArrayList.add(e): %.2f ms\n", arrAddTime / nanoToMilli);

        long listAddTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            list.add(obj);
        }
        long listAddTimeFinish = System.nanoTime();
        long listAddTime = listAddTimeFinish - listAddTimeStart;
        write(fileName, format("LinkedList.add(e): %.2f ms\n", listAddTime / nanoToMilli));
        System.out.printf("LinkedList.add(e): %.2f ms\n", listAddTime / nanoToMilli);

        write(fileName, format("AL/LL = %.2f / 1\n", arrAddTime / (double) listAddTime));
        System.out.printf("AL/LL = %.2f / 1\n", arrAddTime / (double) listAddTime);

//        System.out.println("AL[1000] = " + AL.get(1000));
//        System.out.println("list[1000] = " + list.get(1000));

        write(fileName, "================================\n");
        System.out.println("================================");

        long arrRmTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            AL.remove(obj);
        }
        long arrRmTimeFinish = System.nanoTime();
        long arrRmTime = arrRmTimeFinish - arrRmTimeStart;
        write(fileName, format("ArrayList.remove(e): %.2f ms\n", arrRmTime / nanoToMilli));
        System.out.printf("ArrayList.remove(e): %.2f ms\n", arrRmTime / nanoToMilli);

        long listRmTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            list.remove(obj);
        }
        long listRmTimeFinish = System.nanoTime();
        long listRmTime = listRmTimeFinish - listRmTimeStart;
        write(fileName, format("LinkedList.remove(e): %.2f ms\n", listRmTime / nanoToMilli));
        System.out.printf("LinkedList.remove(e): %.2f ms\n", listRmTime / nanoToMilli);

        write(fileName, format("AL/LL = %.2f / 1\n", arrRmTime / (double) listRmTime));
        System.out.printf("AL/LL = %.2f / 1\n", arrRmTime / (double) listRmTime);

//        try {
//            System.out.println("AL[1000] = " + AL.get(1000));
//        } catch (IndexOutOfBoundsException e){
//            System.out.println("AL[1000] removed");
//        }
//        try {
//            System.out.println("list[1000] = " + list.get(1000));
//        } catch (IndexOutOfBoundsException e){
//            System.out.println("list[1000] removed");
//        }

        write(fileName, "================================\n");
        System.out.print("================================\n");

    }

    public static void addRmToMiddle() {
        List<String> list = new LinkedList<>();
        List<String> AL = new ArrayList<>();

        write(fileName, "Add To Middle\n");
        write(fileName, "================================\n");
        System.out.println("Add To Middle");
        System.out.println("================================");

        long arrAddTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            AL.add(AL.size() / 2, obj);
        }
        long arrAddTimeFinish = System.nanoTime();
        long arrAddTime = arrAddTimeFinish - arrAddTimeStart;
        write(fileName, format("ArrayList.add(e): %.2f ms\n", arrAddTime / nanoToMilli));
        System.out.printf("ArrayList.add(e): %.2f ms\n", arrAddTime / nanoToMilli);

        long listAddTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            list.add(list.size() / 2, obj);
        }
        long listAddTimeFinish = System.nanoTime();
        long listAddTime = listAddTimeFinish - listAddTimeStart;
        write(fileName, format("LinkedList.add(e): %.2f ms\n", listAddTime / nanoToMilli));
        System.out.printf("LinkedList.add(e): %.2f ms\n", listAddTime / nanoToMilli);

        write(fileName, format("AL/LL = %.2f / 1\n", arrAddTime / (double) listAddTime));
        System.out.printf("AL/LL = %.2f / 1\n", arrAddTime / (double) listAddTime);

//        System.out.println("AL[1000] = " + AL.get(1000));
//        System.out.println("list[1000] = " + list.get(1000));

        write(fileName, "================================\n");
        System.out.println("================================");

        long arrRmTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            AL.remove(obj);
        }
        long arrRmTimeFinish = System.nanoTime();
        long arrRmTime = arrRmTimeFinish - arrRmTimeStart;
        write(fileName, format("ArrayList.remove(e): %.2f ms\n", arrRmTime / nanoToMilli));
        System.out.printf("ArrayList.remove(e): %.2f ms\n", arrRmTime / nanoToMilli);

        long listRmTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            list.remove(obj);
        }
        long listRmTimeFinish = System.nanoTime();
        long listRmTime = listRmTimeFinish - listRmTimeStart;
        write(fileName, format("LinkedList.remove(e): %.2f ms\n", listRmTime / nanoToMilli));
        System.out.printf("LinkedList.remove(e): %.2f ms\n", listRmTime / nanoToMilli);

        write(fileName, format("AL/LL = %.2f / 1\n", arrRmTime / (double) listRmTime));
        System.out.printf("AL/LL = %.2f / 1\n", arrRmTime / (double) listRmTime);

//        try {
//            System.out.println("AL[1000] = " + AL.get(1000));
//        } catch (IndexOutOfBoundsException e){
//            System.out.println("AL[1000] removed");
//        }
//        try {
//            System.out.println("list[1000] = " + list.get(1000));
//        } catch (IndexOutOfBoundsException e){
//            System.out.println("list[1000] removed");
//        }

        write(fileName, "================================\n");
        System.out.print("================================\n");

    }

    public static void addRmToEnd() {
        List<String> list = new LinkedList<>();
        List<String> AL = new ArrayList<>();

        write(fileName, "Add To End\n");
        write(fileName, "================================\n");
        System.out.println("Add To End");
        System.out.println("================================");

        long arrAddTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            AL.add(AL.size(), obj);
        }
        long arrAddTimeFinish = System.nanoTime();
        long arrAddTime = arrAddTimeFinish - arrAddTimeStart;
        write(fileName, format("ArrayList.add(e): %.2f ms\n", arrAddTime / nanoToMilli));
        System.out.printf("ArrayList.add(e): %.2f ms\n", arrAddTime / nanoToMilli);

        long listAddTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            list.add(list.size(), obj);
        }
        long listAddTimeFinish = System.nanoTime();
        long listAddTime = listAddTimeFinish - listAddTimeStart;
        write(fileName, format("LinkedList.add(e): %.2f ms\n", listAddTime / nanoToMilli));
        System.out.printf("LinkedList.add(e): %.2f ms\n", listAddTime / nanoToMilli);

        write(fileName, format("AL/LL = %.2f / 1\n", arrAddTime / (double) listAddTime));
        System.out.printf("AL/LL = %.2f / 1\n", arrAddTime / (double) listAddTime);

//        System.out.println("AL[1000] = " + AL.get(1000));
//        System.out.println("list[1000] = " + list.get(1000));

        write(fileName, "================================\n");
        System.out.println("================================");

        long arrRmTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            AL.remove(obj);
        }
        long arrRmTimeFinish = System.nanoTime();
        long arrRmTime = arrRmTimeFinish - arrRmTimeStart;
        write(fileName, format("ArrayList.remove(e): %.2f ms\n", arrRmTime / nanoToMilli));
        System.out.printf("ArrayList.remove(e): %.2f ms\n", arrRmTime / nanoToMilli);

        long listRmTimeStart = System.nanoTime();
        for (int i = 0; i < addRmIndex; i++) {
            list.remove(obj);
        }
        long listRmTimeFinish = System.nanoTime();
        long listRmTime = listRmTimeFinish - listRmTimeStart;
        write(fileName, format("LinkedList.remove(e): %.2f ms\n", listRmTime / nanoToMilli));
        System.out.printf("LinkedList.remove(e): %.2f ms\n", listRmTime / nanoToMilli);

        write(fileName, format("AL/LL = %.2f / 1\n", arrRmTime / (double) listRmTime));
        System.out.printf("AL/LL = %.2f / 1\n", arrRmTime / (double) listRmTime);

//        try {
//            System.out.println("AL[1000] = " + AL.get(1000));
//        } catch (IndexOutOfBoundsException e){
//            System.out.println("AL[1000] removed");
//        }
//        try {
//            System.out.println("list[1000] = " + list.get(1000));
//        } catch (IndexOutOfBoundsException e){
//            System.out.println("list[1000] removed");
//        }

        write(fileName, "================================\n");
        System.out.print("================================\n");

    }

    public static void main(String[] args) throws FileNotFoundException {
        deleteFile(fileName);
        addRmToBegin();
        addRmToMiddle();
        addRmToEnd();
    }
}
