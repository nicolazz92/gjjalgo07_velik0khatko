package main.java.com.getjavajob.training.algo07.velikohatkon.lesson09;

/**
 * Created by ??????? on 05.02.2016.
 * on 15:43
 */
public class CityLookUpStorageTest {

    static CityLookUpStorage map = new CityLookUpStorage();

    public static void addTest() {
        map.add("Moscow");
        map.add("Mogilev");
        map.add("Brest");
        map.add("Barnaul");
        map.add("Novomoskowsk");
        map.add("Novorossiysk");
    }

    public static void searchBySubTest() {
        System.out.println(map.searchBySub("nov").values());
    }

    public static void search() {
        map.search("bar");
    }

    public static void main(String[] args) {
        addTest();
        searchBySubTest();
        search();
        map.printStorage();
    }
}
