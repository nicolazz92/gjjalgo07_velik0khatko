package main.java.com.getjavajob.training.algo07.velikohatkon.lesson04;

import main.java.com.getjavajob.training.algo07.util.Assert;

import java.util.NoSuchElementException;

/**
 * Created by nicolas on 09.10.15.
 * at 12:22
 */
public class SinglyLinkedListTest {
    public static void addtoArray(){
        System.out.println("addtoArray");
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        d.add("one");
        d.add("two");
        d.add("three");
        String str = "";
        System.out.println(d.size());
        for(Object a: d.toArray()){
            str += a;
            str += " ";
        }
        Assert.assertEquals(str, "one two three ");
    }

    public static void isEmpty(){
        System.out.println("isEmpty");
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        Assert.assertEquals(d.isEmpty(), true);
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.isEmpty(), false);
    }

    public static void contains(){
        System.out.println("contains");
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        Assert.assertEquals(d.contains("two"), false);
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.contains("one"), true);
        Assert.assertEquals(d.contains("two"), true);
        Assert.assertEquals(d.contains("three"), true);
    }

    public static void remove(){
        System.out.println("remove");
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        d.add("zero");
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.remove("one"), true);
        Assert.assertEquals(d.remove("two"), true);
        Assert.assertEquals(d.remove("three"), true);
        Assert.assertEquals(d.remove("four"), false);
    }

    public static void get(){
        System.out.println("get");
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.get(0), "one");
        Assert.assertEquals(d.get(1), "two");
        Assert.assertEquals(d.get(2), "three");
        try{
            Assert.assertEquals(d.get(3), "four");
        }catch (ArrayIndexOutOfBoundsException e){
            Assert.assertEquals(e.getMessage(), "get(int i): i is too big");
        }
    }

    public static void set(){
        System.out.println("set");
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        d.add("");
        d.add("");
        d.add("");
        Assert.assertEquals(d.set(0, "one"), "one");
        Assert.assertEquals(d.set(1, "two"), "two");
        Assert.assertEquals(d.set(2, "three"), "three");
        try{
            Assert.assertEquals(d.set(3, "four"), "four");
        }catch (ArrayIndexOutOfBoundsException e){
            Assert.assertEquals(e.getMessage(), "set(int i, Object o): i is too big");
        }
    }

    public static void addIo(){
        System.out.println("addIo");
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        d.add(0, "one");
        d.add(0, "zero");
        d.add(2, "three");
        d.add(2, "two");
        try{
            d.add(5, "three");
        }catch (ArrayIndexOutOfBoundsException e){
            Assert.assertEquals(e.getMessage(), "void add(int i, Object o): i is too big");
        }
        String str = "";
        System.out.println("list size = " + d.size());
        for(Object a: d.toArray()){
            str += a;
            str += " ";
        }
        Assert.assertEquals(str, "zero one two three ");
    }

    public static void indexOf(){
        System.out.println("indexOf");
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        try{
            d.indexOf("zero");
        }catch (NoSuchElementException e){
            Assert.assertEquals(e.getMessage(), "list is empty");
        }
        d.add("zero");
        d.add("one");
        d.add("two");
        d.add("three");
        Assert.assertEquals(d.indexOf("zero"), 0);
        Assert.assertEquals(d.indexOf("one"), 1);
        Assert.assertEquals(d.indexOf("two"), 2);
        Assert.assertEquals(d.indexOf("three"), 3);
        try{
            Assert.assertEquals(d.indexOf("four"), -1);
        }catch (NoSuchElementException e){
            Assert.assertEquals(e.getMessage(), "element has not being found");
        }
    }
    //////////////////////////////////////////////////////////////////////iterator

    public static void iterAddHasNext(){
        System.out.println("iterHasNext");
        SinglyLinkedList.ListIteratorImpl<String> iterator = new SinglyLinkedList.ListIteratorImpl<>();
        iterator.add("one");
        iterator.add("two");
        iterator.add("three");

        if(iterator.hasNext()){
            Assert.assertEquals(iterator.next(), "two");
        }
        if(iterator.hasNext()){
            Assert.assertEquals(iterator.next(), "three");
        }
        if(iterator.hasNext()){
            Assert.assertEquals(iterator.next(), "four");
        }
    }
    public static void iterHasPrevious(){
        System.out.println("iterHasPrevious");
        SinglyLinkedList.ListIteratorImpl<String> iterator = new SinglyLinkedList.ListIteratorImpl<>();
        iterator.add("one");
        iterator.add("two");
        iterator.add("three");
        iterator.next();
        iterator.next();
        if(iterator.hasPrevious()){
            Assert.assertEquals(iterator.previous(), "two");
        }
        if(iterator.hasPrevious()){
            Assert.assertEquals(iterator.previous(), "one");
        }
        if(iterator.hasPrevious()){
            Assert.assertEquals(iterator.previous(), "zero");
        }
    }

    public static void iterNextPreviousIndex(){
        System.out.println("iterNextPreviousIndex");
        SinglyLinkedList.ListIteratorImpl<String> iterator = new SinglyLinkedList.ListIteratorImpl<>();
        iterator.add("one");
        iterator.add("two");
        iterator.add("three");
        if(iterator.hasNext()){
            Assert.assertEquals(iterator.nextIndex(), 1);
        }
        if(iterator.hasNext()){
            Assert.assertEquals(iterator.nextIndex(), 2);
        }
        if(iterator.hasNext()){
            Assert.assertEquals(iterator.nextIndex(), 3);
        }
    }

    public static void iterRemove() {
        System.out.println("iterRemove");
        SinglyLinkedList.ListIteratorImpl<String> iterator = new SinglyLinkedList.ListIteratorImpl<>();
        iterator.add("one");
        iterator.add("two");
        iterator.add("three");
        if (iterator.hasNext()) {
            Assert.assertEquals(iterator.nextIndex(), 1);
        }
        iterator.remove();
        if (iterator.hasNext()) {
            Assert.assertEquals(iterator.nextIndex(), 3);
        }
    }

    public static void iterSet(){
        System.out.println("iterSet");
        SinglyLinkedList.ListIteratorImpl<String> iterator = new SinglyLinkedList.ListIteratorImpl<>();
        iterator.add("one");
        iterator.add("two");
        iterator.add("three");
        if(iterator.hasNext()){
            iterator.next();
        }
        iterator.set("twoSetted");
        iterator.previous();
        iterator.previous();
        if(iterator.hasNext()) {
            Assert.assertEquals(iterator.next(), "one");
        }
        if(iterator.hasNext()) {
            Assert.assertEquals(iterator.next(), "twoSetted");
        }
        if(iterator.hasNext()) {
            Assert.assertEquals(iterator.next(), "three");
        }
    }

    public static void testAdd(SinglyLinkedList<String> d) {
        d.add("zero");
        d.add("one");
        d.add("two");
        d.add("three");
        d.add("four");
        d.add("five");
        d.add("six");
        d.add("seven");
        d.add("eight");
        d.add("nine");
    }

    public static String print(SinglyLinkedList<String> d) {
        String res = "";
        for (Object a : d.toArray()) {
            res += a + " ";
            System.out.print(a + " ");
        }
        System.out.println();
        return res;
    }

    public static void reverse(){
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        testAdd(d);
        print(d);
        d.reverse();
        Assert.assertEquals(print(d), "nine eight seven six five four three two one zero ");
    }

    public static void swap1(){
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        testAdd(d);
        d.swap(0, 1);
        Assert.assertEquals(print(d), "one zero two three four five six seven eight nine ");
    }

    public static void swap2(){
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        testAdd(d);
        d.swap(0, 9);
        Assert.assertEquals(print(d), "nine one two three four five six seven eight zero ");
    }

    public static void swap3(){
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        testAdd(d);
        d.swap(0, 5);
        Assert.assertEquals(print(d), "five one two three four zero six seven eight nine ");
    }

    public static void swap4(){
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        testAdd(d);
        d.swap(3, 4);
        Assert.assertEquals(print(d), "zero one two four three five six seven eight nine ");
    }

    public static void swap5(){
        SinglyLinkedList<String> d = new SinglyLinkedList<>();
        testAdd(d);
        d.swap(2, 4);
        Assert.assertEquals(print(d), "zero one four three two five six seven eight nine ");
    }

    public static void main(String[] args) {
        addtoArray();
        isEmpty();
        contains();
        remove();
        get();
        set();
        addIo();
        indexOf();
        iterAddHasNext();
        iterHasPrevious();
        iterNextPreviousIndex();
        iterRemove();
        iterSet();
        reverse();
        swap1();
        swap2();
        swap3();
        swap4();
        swap5();
    }
}
