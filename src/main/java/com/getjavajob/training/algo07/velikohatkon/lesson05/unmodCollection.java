package main.java.com.getjavajob.training.algo07.velikohatkon.lesson05;

import main.java.com.getjavajob.training.algo07.velikohatkon.lesson04.SinglyLinkedList;

/**
 * Created by ������� on 06.11.2015.
 * on 16:32
 */
public class unmodCollection<E> extends SinglyLinkedList<E> {

    SinglyLinkedList<E> temp;

    public unmodCollection(SinglyLinkedList<E> origColl) {
        temp = origColl;
    }

    @Override
    public int size() {
        return temp.size();
    }

    @Override
    public boolean isEmpty() {
        return temp.getHead().val == null;
    }

    @Override
    public boolean contains(Object o) {
        return temp.contains(o);
    }

    @Override
    public Object[] toArray() {
        return temp.toArray();
    }

    @Override
    public E get(int i) {
        return temp.get(i);
    }

    @Override
    public int indexOf(Object o) {
        return temp.indexOf(o);
    }

    @Override
    public Node<E> getElem(int num) {
        return temp.getElem(num);
    }

    @Override
    public String print() {
        return temp.print();
    }

    @Override
    public boolean add(E o) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("collection is unmodifiable");
    }

    @Override
    public boolean remove(Object o) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("collection is unmodifiable");
    }

    @Override
    public E set(int i, E o) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("collection is unmodifiable");
    }

    @Override
    public void add(int i, E o) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("collection is unmodifiable");
    }

    @Override
    public E remove(int i) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("collection is unmodifiable");
    }

    @Override
    public boolean swap(int sw1, int sw2) throws UnsupportedOperationException {
        throw new UnsupportedOperationException("collection is unmodifiable");
    }

    @Override
    public boolean reverse() throws UnsupportedOperationException {
        throw new UnsupportedOperationException("collection is unmodifiable");
    }

}