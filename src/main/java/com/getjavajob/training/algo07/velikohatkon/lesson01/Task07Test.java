package main.java.com.getjavajob.training.algo07.velikohatkon.lesson01;

/**
 * Created by ������� on 11.08.2015.
 * on 10:46
 */
public class Task07Test {
    public static void testAr1() {
        Task07 t = new Task07(2, 5);
        t.flipAr1();
        t.test(5, 2);
    }

    public static void testAr2() {
        Task07 t = new Task07(2, 5);
        t.flipAr2();
        t.test(5, 2);
    }

    public static void testBit1() {
        Task07 t = new Task07(14, 7);
        t.flipBit1();
        t.test(7, 14);
    }

    public static void testBit2() {
        Task07 t = new Task07(14, 7);
        t.flipBit2();
        t.test(7, 14);
    }

    public static void main(String[] args) {
        testAr1();
        testAr2();
        testBit1();
        testBit2();
    }
}
