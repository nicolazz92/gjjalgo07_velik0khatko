package main.java.com.getjavajob.training.algo07.velikohatkon.lesson05;

import main.java.com.getjavajob.training.algo07.util.Assert;

import java.util.EmptyStackException;

/**
 * Created by ������� on 25.10.2015.
 * on 16:51
 */
public class LinkedListStackTest {

    public static void pushTest(){
        LinkedListStack<String> d = new LinkedListStack<>();
        d.push("two");
        d.push("one");
        d.push("zero");
        Assert.assertEquals(d.print(), "zero one two ");
    }

    public static void popTest(){
        LinkedListStack<String> d = new LinkedListStack<>();
        d.push("two");
        d.push("one");
        d.push("zero");
        Assert.assertEquals(d.pop(), "zero");
        Assert.assertEquals(d.pop(), "one");
        Assert.assertEquals(d.pop(), "two");
        try{
            d.pop();
        } catch (EmptyStackException e){
            System.out.println("PASSED: EmptyStackException caught");
        }
    }

    public static void main(String[] args) {
        pushTest();
        popTest();
    }
}
