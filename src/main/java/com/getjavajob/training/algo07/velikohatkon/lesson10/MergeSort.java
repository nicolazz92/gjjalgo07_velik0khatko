package main.java.com.getjavajob.training.algo07.velikohatkon.lesson10;

/**
 * Created by Николай on 29.02.2016
 * at 21:20
 */
public class MergeSort<E> {
    public void sorting(E[] arr) {
        E[] tempArray = (E[]) new Object[arr.length];
        mergeSort(arr, tempArray, 0, arr.length - 1);
    }

    private void mergeSort(E[] arr, E[] tempArray, int lowerIndex, int upperIndex) {
        if (!(lowerIndex == upperIndex)) {
            int mid = (lowerIndex + upperIndex) / 2;
            mergeSort(arr, tempArray, lowerIndex, mid);
            mergeSort(arr, tempArray, mid + 1, upperIndex);
            merge(arr, tempArray, lowerIndex, mid + 1, upperIndex);
        }
    }

    private void merge(E[] arr, E[] tempArray, int lowerIndexCursor, int higerIndex, int upperIndex) {
        int tempIndex = 0;
        int lowerIndex = lowerIndexCursor;
        int midIndex = higerIndex - 1;
        int totalItems = upperIndex - lowerIndex + 1;
        while (lowerIndex <= midIndex && higerIndex <= upperIndex) {
            if (arr[lowerIndex].hashCode() < arr[higerIndex].hashCode()) {
                tempArray[tempIndex++] = arr[lowerIndex++];
            } else {
                tempArray[tempIndex++] = arr[higerIndex++];
            }
        }

        while (lowerIndex <= midIndex) {
            tempArray[tempIndex++] = arr[lowerIndex++];
        }

        while (higerIndex <= upperIndex) {
            tempArray[tempIndex++] = arr[higerIndex++];
        }

        System.arraycopy(tempArray, 0, arr, lowerIndexCursor, totalItems);
    }
}
