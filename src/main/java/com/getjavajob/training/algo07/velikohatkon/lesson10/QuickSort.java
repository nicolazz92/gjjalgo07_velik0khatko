package main.java.com.getjavajob.training.algo07.velikohatkon.lesson10;

/**
 * Created by Николай on 29.02.2016
 * at 17:26
 */
public class QuickSort<E> {

    public void sorting(E[] arr) {
        quickSort(arr, 0, arr.length - 1);
    }

    int partition(E arr[], int left, int right) {
        int i = left, j = right;
        E tmp;
        E pivot = arr[(left + right) / 2];

        while (i <= j) {
            while (arr[i].hashCode() < pivot.hashCode())
                i++;
            while (arr[j].hashCode() > pivot.hashCode())
                j--;
            if (i <= j) {
                tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;
                i++;
                j--;
            }
        }
        return i;
    }

    void quickSort(E arr[], int left, int right) {
        int index = partition(arr, left, right);
        if (left < index - 1)
            quickSort(arr, left, index - 1);
        if (index < right)
            quickSort(arr, index, right);
    }
}
