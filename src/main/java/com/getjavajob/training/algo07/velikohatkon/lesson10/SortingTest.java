package main.java.com.getjavajob.training.algo07.velikohatkon.lesson10;

import java.util.Arrays;

/**
 * Created by Николай on 29.02.2016
 * at 16:26
 */
public class SortingTest {

    public static void bubbleTest(){
        BubbleSort<Integer> bubble = new BubbleSort<>();
        Integer[] array = {6, 3, 5, 2, 4, 1};
        bubble.sorting(array);
        System.out.println(Arrays.toString(array));
    }

    public static void insertionTest(){
        InsertionSort<Integer> insert = new InsertionSort<>();
        Integer[] array = {6, 3, 5, 2, 4, 1};
        insert.sorting(array);
        System.out.println(Arrays.toString(array));
    }

    public static void quickTest(){
        QuickSort<Integer> quick = new QuickSort<>();
        Integer[] array = {6, 3, 5, 2, 4, 1};
        quick.sorting(array);
        System.out.println(Arrays.toString(array));
    }

    public static void mergeTest(){
        MergeSort<Integer> merge = new MergeSort<>();
        Integer[] array = {6, 3, 5, 2, 4, 1};
        merge.sorting(array);
        System.out.println(Arrays.toString(array));
    }

    public static void speedTestOuickVSMerge(int n){
        Integer[] testArr;
        double nanoToMilli = 1_000_000;

        testArr = new Integer[n];
        for(int i=0;i < testArr.length;i++){
            testArr[i] = (int)(Math.random() * n);
        }

        long OuickTimeStart = System.nanoTime();

        QuickSort<Integer> quick = new QuickSort<>();
        quick.sorting(testArr);

        long OuickTimeFinish = System.nanoTime();
        long OuickTime = OuickTimeFinish - OuickTimeStart;
        System.out.printf("Quick Sorting: %.2f ms\n\n", OuickTime / nanoToMilli);

        ///////////////////////////////////////////////
        testArr = new Integer[n];
        for(int i=0;i < testArr.length;i++){
            testArr[i] = (int)(Math.random() * n);
        }

        long MergeTimeStart = System.nanoTime();

        MergeSort<Integer> merge = new MergeSort<>();
        merge.sorting(testArr);

        long MergeTimeFinish = System.nanoTime();
        long MergeTime = MergeTimeFinish - MergeTimeStart;
        System.out.printf("Merge Sorting: %.2f ms\n", MergeTime / nanoToMilli);

    }

    public static void main(String[] args) {
//        bubbleTest();
//        insertionTest();
//        quickTest();
        mergeTest();
//        speedTestOuickVSMerge(25_000_000);
    }
}
