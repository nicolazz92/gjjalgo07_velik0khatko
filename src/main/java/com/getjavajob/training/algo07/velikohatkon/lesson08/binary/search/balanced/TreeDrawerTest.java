package main.java.com.getjavajob.training.algo07.velikohatkon.lesson08.binary.search.balanced;

import main.java.com.getjavajob.training.algo07.util.Assert;

import java.util.Arrays;

/**
 * Created by ??????? on 01.02.2016.
 * on 9:40
 */
public class TreeDrawerTest {
    public static void heightTest() {
        TreeDrawer<Integer> d = new TreeDrawer<>();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(200);

        System.out.println(d.height(d.root()));
    }

    public static void addSpaceBeforeTest() {
        TreeDrawer<Integer> d = new TreeDrawer<>();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(200);

        String a;
        a = d.addSpaceBefore(2);
        Assert.assertEquals(a, " ");
    }

    public static void addSpaceAfterTest() {
        TreeDrawer<Integer> d = new TreeDrawer<>();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(200);

        String a;
        a = d.addSpaceAfter(2);
        Assert.assertEquals(a, "  ");
    }

    public static void setTreeLevelStructTest() {
        TreeDrawer<Integer> d = new TreeDrawer<>();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(200);

        d.setTreeLevelStruct();
    }

    public static void levelSpliterTest() {
        TreeDrawer<?> d = new TreeDrawer<>();
        String a = "    12       43    89   ";
        Assert.assertEquals(Arrays.toString(new String[]{"12", "43", "89"}), Arrays.toString(d.levelSpliter(a)));
    }

    public static void treeArrayFillingStringTest() {
        TreeDrawer<String> d = new TreeDrawer<>();

        d.add(String.valueOf('5'));
        d.add(String.valueOf('3'));
        d.add(String.valueOf('7'));
        d.add(String.valueOf('8'));
        d.add(String.valueOf('9'));
        d.add(String.valueOf('a'));
        d.add(String.valueOf('0'));

        d.rotate(d.treeSearch(d.root(), "7"));
        d.rotate(d.treeSearch(d.root(), "9"));
//        d.rotate(d.treeSearch(d.root(), "3"));
        d.rotate(d.treeSearch(d.root(), "91"));

        d.treePrint();
    }

    public static void treeArrayFillingIntegerTest() {
        TreeDrawer<Integer> d = new TreeDrawer<>();

        d.add(100);
        d.add(50);
        d.add(150);
        d.add(200);
        d.add(300);

//        d.rotate(d.treeSearch(d.root(), "7"));
//        d.rotate(d.treeSearch(d.root(), "9"));
//        d.rotate(d.treeSearch(d.root(), "3"));
//        d.rotate(d.treeSearch(d.root(), "91"));

        d.treePrint();
    }

    public static void main(String[] args) {
        heightTest();
        setTreeLevelStructTest();
        levelSpliterTest();
        treeArrayFillingIntegerTest();
        treeArrayFillingStringTest();
        addSpaceBeforeTest();
        addSpaceAfterTest();
    }
}