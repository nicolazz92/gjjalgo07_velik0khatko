package main.java.com.getjavajob.training.algo07.velikohatkon.lesson01;

import main.java.com.getjavajob.training.algo07.util.Assert;

/**
 * Created by ������� on 11.08.2015.
 * on 10:46
 */
public class Task06Test {
    public static void testA() {
        Assert.assertEquals(Task06.a(4), 16);
    }

    public static void testB() {
        Assert.assertEquals(Task06.b(4, 3), 24);
    }

    public static void testC() {
        Assert.assertEquals(Task06.c(31, 2), 28);
    }

    public static void testD() {
        Assert.assertEquals(Task06.d(30, 1), 31);
    }

    public static void testE() {
        Assert.assertEquals(Task06.e(31, 1), 30);
        Assert.assertEquals(Task06.e(30, 1), 31);
    }

    public static void testF() {
        Assert.assertEquals(Task06.f(31, 2), 29);
    }

    public static void testG() {
        Assert.assertEquals(Task06.g(31, 4), 7);
    }

    public static void testH() {
        Assert.assertEquals(Task06.h(0b111010, 2), 1);
    }

    public static void testI() {
        Assert.assertEquals(Task06.i(0b01001101), 1001101);
    }

    public static void main(String[] args) {
        testA();
        testB();
        testC();
        testD();
        testE();
        testF();
        testG();
        testH();
        testI();
    }
}
