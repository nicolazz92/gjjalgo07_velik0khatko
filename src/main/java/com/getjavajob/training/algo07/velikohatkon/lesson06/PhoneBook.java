package main.java.com.getjavajob.training.algo07.velikohatkon.lesson06;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by ������� on 27.11.2015.
 * on 17:20
 */
public class PhoneBook {

    public static String name() {
        System.out.println("Enter name: ");
        Scanner nameScan = new Scanner(System.in);
        String name = nameScan.nextLine();
        if (!name.equals("")) {
            return name;
        } else {
            return name();
        }
    }

    public static long phone() {
        System.out.println("Enter phone: ");
        Scanner phoneScan = new Scanner(System.in);
        Long phone;
        try {
            phone = phoneScan.nextLong();
        } catch (InputMismatchException e) {
            return phone();
        }
        return phone;
    }

    public static void sleep(int t){
        try {
            Thread.sleep(t);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void phoneBook() {
        AssociativeArray<String, Long> store = new AssociativeArray<>();
        int choice;

        do {
            System.out.println(
                    "\n1: Enter new person \n" +
                            "2: Get phone \n" +
                            "3: Exit \n" +
                            "Enter the number of operation: "
            );

            Scanner choiceScan = new Scanner(System.in);
            choice = choiceScan.nextInt();
            if (choice == 1) {
                store.add(name(), phone());
                System.out.println("Thanks");
                sleep(1000);
            } else if (choice == 2) {
                System.out.println("Enter the name of person: ");
                Scanner nameScan = new Scanner(System.in);
                String name = nameScan.nextLine();
                Long resultPhone = store.get(name);
                if(resultPhone!= null){
                    System.out.println("Phone: " + resultPhone);
                } else {
                    System.out.println("No searching person");
                }
                sleep(1000);
            } else if (choice == 3) {
                break;
            }
        } while (true);
    }

    public static void main(String[] args) {
        phoneBook();
    }
}
