package main.java.com.getjavajob.training.algo07.velikohatkon.lesson06;

/**
 * Created by ������� on 22.11.2015.
 * on 13:15
 */
public class AssociativeArray<K, V> {
    private int elemCount;
    private boolean growFlag = false;

    private Node[] mas = new Node[10];

    public class Node<P, G> {
        public Node<K, V> next;
        public P key;
        public G val;

        public Node(P key, G val) {
            this.key = key;
            this.val = val;
        }

        public P getKey() {
            return key;
        }

        public G getVal() {
            return val;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Node<?, ?> node = (Node<?, ?>) o;

            return !(key != null ? !key.equals(node.key) : node.key != null);
        }

        @Override
        public int hashCode() {
            if (String.class.isInstance(key)) {
                String value = (String)key;
                long h = 0;
                for (int i = 0; i < value.length(); i++) {
                    h = h + value.charAt(i);
                }
                return key != null ? new Long(h).hashCode() % mas.length : 0;
            }
            return key != null ? key.hashCode() % mas.length : 0;
        }

        //        public int hashCode() {
//            if (key == null) {
//                return 0;
//            }
//
//            int hashNumber = 0;
//            if (Integer.class.isInstance(key)) {
//                hashNumber = Math.abs((Integer) key % mas.length);
//            }
//            if (Character.class.isInstance(key)) {
//                hashNumber = Math.abs((Character) key % mas.length);
//            }
//            if (Byte.class.isInstance(key)) {
//                hashNumber = Math.abs((Byte) key % mas.length);
//            }
//            if (Short.class.isInstance(key)) {
//                hashNumber = Math.abs((Short) key % mas.length);
//            }
//            if (Long.class.isInstance(key)) {
//                hashNumber = (int) Math.abs((Long) key % mas.length);
//            }
//            if (Float.class.isInstance(key)) {
//                int multiplex = String.valueOf(key).length() - (String.valueOf(key).indexOf('.') + 1);
//                int multi = 1;
//                for (int i = 0; i < multiplex; i++) {
//                    multi *= 10;
//                }
//                hashNumber = Math.round(Math.abs((Float) key * multi % mas.length));
//            }
//            if (Double.class.isInstance(key)) {
//                int multiplex = String.valueOf(key).length() - (String.valueOf(key).indexOf('.') + 1);
//                int multi = 1;
//                for (int i = 0; i < multiplex; i++) {
//                    multi *= 10;
//                }
//                hashNumber = (int) Math.round(Math.abs((Double) key * multi % mas.length));
//            }
//            if (String.class.isInstance(key)) {
//                hashNumber = String.valueOf(key).hashCode() % mas.length;
//            }
//            return hashNumber;
//        }

    }

    public AssociativeArray() {
        mas = new Node[10];
        elemCount = 0;
    }

    public int getSize() {
        return elemCount;
    }

    private V searchEqualsKey(Node<K, V> o, int hashNum) {
        Node d = mas[hashNum];
        do {
            if (d.equals(o)) {
                V res = (V) d.val;
                d.val = o.val;
                return res;
            }
            d = d.next;
        } while (d != null);
        return null;
    }

    private V addToList(Node<K, V> o, int hashNum) {
        Node d = mas[hashNum];
        V searchRes = searchEqualsKey(o, hashNum);
        if (searchRes != null) {
            return searchRes;
        }
        while (d.next != null) {
            d = d.next;
        }
        d.next = o;
        elemCount++;
        return null;
    }

    public V add(K key, V val) {
        if (!growFlag) {
            growIfFull();
        }
        Node<K, V> o = new Node<>(key, val);
        int hashNum = o.hashCode();
        if (mas[hashNum] == null) {
            mas[hashNum] = o;
            elemCount++;
        } else {
            return addToList(o, hashNum);
        }
        return null;
    }

    public V get(K key) {
        Node<K, Object> o = new Node<>(key, null);
        int hashNum = o.hashCode();
        Node d = mas[hashNum];
        while (d != null) {
            if (d.equals(o)) {
                return (V) d.val;
            }
            d = d.next;
        }
        return null;
    }

    public V remove(K key) {
        Node<K, Object> o = new Node<>(key, null);
        int hashNum = o.hashCode();
        Node d = mas[hashNum];
        V res;
        if (mas[hashNum].equals(o)) {
            res = (V) mas[hashNum].val;
            mas[hashNum] = mas[hashNum].next;
            elemCount--;
            return res;
        }
        while (d.next != null) {
            if (d.next.equals(o)) {
                res = (V) d.next.val;
                d.next = d.next.next;
                elemCount--;
                return res;
            }
            d = d.next;
        }
        return null;
    }

    private void growIfFull() {
        if (elemCount > mas.length / 2) {
            growFlag = true;
            Node<K, V>[] masBuffer = mas;
            elemCount = 0;
            mas = new Node[(int) Math.round(masBuffer.length * 1.5)];
            for (Node<K, V> masBuf : masBuffer) {
                if (masBuf != null) {
                    do {
                        add(masBuf.getKey(), masBuf.getVal());
                        masBuf = masBuf.next;
                    } while (masBuf != null);
                }
            }
            growFlag = false;
        }
    }

    public void printMass() {
        for (int i = 0; i < mas.length; i++) {
            System.out.println(i);
            if (mas[i] != null) {
                Node<K, V> d = mas[i];
                while (d != null) {
                    System.out.println("  key: " + String.valueOf(d.key) + "  val: " + String.valueOf(d.val));
                    d = d.next;
                }
            }
        }
    }

}