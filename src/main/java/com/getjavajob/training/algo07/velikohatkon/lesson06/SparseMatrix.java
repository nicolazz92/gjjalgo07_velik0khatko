package main.java.com.getjavajob.training.algo07.velikohatkon.lesson06;

/**
 * Created by ������� on 28.11.2015.
 * on 12:48
 */
public class SparseMatrix {
    private AssociativeArray<String, Integer> matrix = new AssociativeArray<>();

    public AssociativeArray<String, Integer> getMatrix() {
        return matrix;
    }

    public Integer add(int column, int line, int data){
        String hashString = "matrix column = " + String.valueOf(column) + ", matrix line = " + String.valueOf(line);
        /*Because of bad (Integer.hashCode + Integer.hashCode) implementation
        * and a tiny area of result values
        * I decided to use String.hashCode */
        return matrix.add(hashString, data);
    }

    public Integer get(int column, int line){
        String hashString = "matrix column = " + String.valueOf(column) + ", matrix line = " + String.valueOf(line);
        return matrix.get(hashString);
    }

    public Integer remove(int column, int line){
        String hashString = "matrix column = " + String.valueOf(column) + ", matrix line = " + String.valueOf(line);
        return matrix.remove(hashString);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SparseMatrix that = (SparseMatrix) o;

        return matrix != null ? matrix.equals(that.matrix) : that.matrix == null;

    }

    @Override
    public int hashCode() {
        return matrix != null ? matrix.hashCode() : 0;
    }
}
