package main.java.com.getjavajob.training.algo07.velikohatkon.lesson05;

import main.java.com.getjavajob.training.algo07.velikohatkon.lesson04.SinglyLinkedList;

/**
 * Created by ������� on 05.11.2015.
 * on 13:01
 */
public class CollectionUtils {

    public static void filterModificating(SinglyLinkedList<Integer> d) {
        int i = 0;
        while (i < d.size()) {
            if (d.get(i) < (Integer) 5) {
                d.remove(i);
            } else {
                i++;
            }
        }
    }

    public static SinglyLinkedList<Integer> filterReturning(SinglyLinkedList<Integer> d) {
        int i = 0;
        SinglyLinkedList<Integer> res = new SinglyLinkedList<>();
        while (i < d.size()) {
            if (d.get(i) >= (Integer) 5) {
                res.add(d.get(i));
            }
            i++;
        }
        return res;
    }

    public static void trasformModificating(SinglyLinkedList d) {
        SinglyLinkedList<Double> res = new SinglyLinkedList<>();
        for (Object aD : d) {
            String buf = String.valueOf(aD);
            float buf1 = Float.valueOf(buf);
            res.add((double) buf1);
        }
        d = res;
        System.out.println(d);
    }

    public static SinglyLinkedList<Double> trasformReturning(SinglyLinkedList<Integer> d) {
        SinglyLinkedList<Double> res = new SinglyLinkedList<>();
        for (Integer aD : d) {
            String buf = String.valueOf(aD);
            float buf1 = Float.valueOf(buf);
            res.add((double) buf1);
        }
        return res;
    }

    public static void forAllDo(SinglyLinkedList<Integer> d) {
        for (int i = 0; i < d.size(); i++) {
            if (d.get(i) < (Integer) 5) {
                d.set(i, d.get(i) * 2);
            }
        }
    }

    public static unmodCollection unmodifiableCollection(SinglyLinkedList origColl) {
        return new unmodCollection(origColl);
    }
}
